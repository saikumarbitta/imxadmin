importScripts('https://www.gstatic.com/firebasejs/4.1.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.1.1/firebase-messaging.js');
importScripts('https://www.gstatic.com/firebasejs/4.1.1/firebase.js');


var config = {
    apiKey: 'AIzaSyB5HCm2-V_KEwzfDZUwbHvnIB7KyPobmhY',
    authDomain: 'careway-for-cvd.firebaseapp.com',
    databaseURL: 'https://careway-for-cvd.firebaseio.com',
    projectId: 'careway-for-cvd',
    storageBucket: 'careway-for-cvd.appspot.com',
    messagingSenderId: '1091233228343'
};
firebase.initializeApp(config);
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    const notificationTitle = 'Background Message Title';
    const notificationOptions = {
        body: 'Background Message body.',
        icon: '/firebase-logo.png'
    };

return self.registration.showNotification(notificationTitle,
    notificationOptions);
});

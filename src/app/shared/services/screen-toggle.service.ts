import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ScreenToggle {

  constructor() { }


  launchIntoFullscreen(element) {
    if (element.requestFullscreen) {
      element.requestFullscreen();
    } else if (element.mozRequestFullScreen) {
      element.mozRequestFullScreen();
    } else if (element.webkitRequestFullscreen) {
      element.webkitRequestFullscreen();
    } else if (element.msRequestFullscreen) {
      element.msRequestFullscreen();
    }
  }

  exitFullscreen() {
    if (document['fullscreenElement'] ||
      document['webkitFullscreenElement'] ||
      document['mozFullScreenElement']) {
      document.exitFullscreen();
    }
    // else if (document['mozFullScreen']) {
    //   document['mozCancelFullScreen']();
    // } else if (document['webkitExitFullscreen']) {
    //   document['webkitExitFullscreen']();
    // }

  }

}

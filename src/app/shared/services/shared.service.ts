import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { IPatientList } from '../../patient/patient-list/patient-list.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { IPatientListDetails } from '../../patient/patient.model';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private API_URL = environment.API_URL;
  private API_URL_COMMENT = environment.API_URL_COMMENT;
  private API_URL2 = environment.API_URL2;
  // private GET_UHID = environment.GET_UHID;
  doctorId: any;
  fcmId: any;
  operator_id: any;
  name: any;
  emailid: any;
  mobile: any;
  operator_type: any;
  cloud_analyser_enabled: any;
  markas_delete: any;

  mode: any;
  pdfDenoiseSync: any;
  sync: any;
  logLevel: any;
  sesTimeout: any;
  sampFreq: any;
  tPrinter: any;
  userType: any;
  spotMonitor: any;
  patientName: any;
  patientAge: any;
  patientPhone: any;
  ecgRep: any;
  qrCode: any;

  countrycode: any;
  hospital_code: any;
  hospital_name: any;
  last_reset_password_time: any;
  terms_condition: any;

  notification_flag: any;
  speciality: any;
  doctor_id: any;
  doctor_name: any;
  doctor_emailid: any;
  doctor_mobile: any;
  doctor_markas_delete: any;

  view: any;
  comment: any;
  forward: any;
  closure: any;
  is_thirdparty_doctor: any;
  is_admin: any;

  cityname: any;
  city_classification: any;
  qualification: any;
  specialityOP: any;
  role: any;
  region: any;

  selectedTab = 0;

  constructor(private http: HttpClient) {
    this.doctorId = localStorage.getItem('doc-app-doctor-id');
  }

  public titleSource = new BehaviorSubject('');
  title = this.titleSource.asObservable();

  setOperator(operator_id , name, emailid,
              mobile, operator_type, cloud_analyser_enabled,
              markas_delete,
              mode, pdfDenoiseSync, sync,
              logLevel, sesTimeout, sampFreq,
              tPrinter, userType, spotMonitor,
              patientName, patientAge, patientPhone,
              ecgRep, qrCode,
            countrycode, hospital_code, hospital_name, last_reset_password_time , terms_condition,
            is_admin,
            cityname, city_classification,
            qualification, speciality,
            role, region) {
    this.operator_id = operator_id;
   // console.log(name);
    this.name = name;
    this.emailid = emailid;
    this.mobile = mobile;
    this.operator_type = operator_type;
    this.cloud_analyser_enabled = cloud_analyser_enabled;
    this.markas_delete = markas_delete;

    this.mode = mode;
    this.pdfDenoiseSync = pdfDenoiseSync;
    this.sync = sync;
    this.logLevel = logLevel;
    this.sesTimeout = sesTimeout;

    this.sampFreq = sampFreq;
    this.tPrinter = tPrinter;
    this.userType = userType;
    this.spotMonitor = spotMonitor;
    this.patientName = patientName;

    this.patientAge = patientAge;
    this.patientPhone = patientPhone;
    this.ecgRep = ecgRep;
    this.qrCode = qrCode;

    this.countrycode = countrycode;
    this.hospital_code = hospital_code;
    this.hospital_name = hospital_name;
    this.last_reset_password_time = last_reset_password_time;
    this.terms_condition = terms_condition;

    this.is_admin = is_admin;

    this.cityname = cityname;
    this.city_classification = city_classification;
    this.qualification = qualification;
    this.specialityOP = speciality;
    this.role = role;
    this.region = region;
  }

  getSelectedOperator() {
    const selectedOperator = { 'operator_id':  this.operator_id,
                  'name': this.name,
                  'emailid':  this.emailid ,
                  'mobile': this.mobile,
                  'operator_type': this.operator_type,
                  'cloud_analyser_enabled': this.cloud_analyser_enabled,
                  'markas_delete': this.markas_delete ,

                  'mode': this.mode,
                  'pdfDenoiseSync': this.pdfDenoiseSync,
                  'sync': this.sync,
                  'logLevel': this.logLevel,
                  'sesTimeout': this.sesTimeout,

                  'sampFreq': this.sampFreq,
                  'tPrinter': this.tPrinter,
                  'userType': this.userType,
                  'spotMonitor': this.spotMonitor,
                  'patientName': this.patientName,

                  'patientAge': this.patientAge,
                  'patientPhone': this.patientPhone,
                  'ecgRep': this.ecgRep,
                  'qrCode': this.qrCode,

                  'countrycode': this.countrycode,
                  'hospital_code': this.hospital_code,
                  'hospital_name': this.hospital_name,
                  'last_reset_password_time': this.last_reset_password_time,
                  'terms_condition': this.terms_condition,
                  'is_admin': this.is_admin,

                  'cityname': this.cityname,
                  'city_classification': this.city_classification,
                  'qualification': this.qualification,
                  'speciality' : this.specialityOP,
                  'role': this.role,
                  'region': this.region
                };

      return selectedOperator ;
}

  setDoctor(doctor_id , name, emailid,
    mobile, operator_type, cloud_analyser_enabled,
    markas_delete,
    view, comment, forward, closure, is_thirdparty_doctor) {
      this.doctor_id = doctor_id;
      this.doctor_name = name;
      this.doctor_emailid = emailid;
      this.doctor_mobile = mobile;
      this.speciality = operator_type;
      this.notification_flag = cloud_analyser_enabled;
      this.doctor_markas_delete = markas_delete;

      this.view = view;
      this.comment = comment;
      this.forward = forward;
      this.closure = closure;
      this.is_thirdparty_doctor = is_thirdparty_doctor;
  }

  getSelectedeDoctor() {
    const selectedeDoctor = { 'doctor_id':  this.doctor_id,
                  'doctor_name': this.doctor_name,
                  'doctor_emailid':  this.doctor_emailid ,
                  'doctor_mobile': this.doctor_mobile,
                  'speciality': this.speciality,
                  'notification_flag': this.notification_flag,
                  'doctor_markas_delete': this.doctor_markas_delete ,

                  'view':  this.view ,
                  'comment': this.comment,
                  'forward': this.forward,
                  'closure': this.closure,
                  'is_thirdparty_doctor': this.is_thirdparty_doctor};

      return selectedeDoctor ;
  }

  setSelectedTab (selectedTab) {
  //  console.log(this.selectedTab );
    this.selectedTab = selectedTab;
  }

  getSelectedTab () {
    // console.log(this.selectedTab );
    return this.selectedTab;
  }
}

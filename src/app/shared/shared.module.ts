import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './components/layout/layout.component';
import { MaterialModule } from '../angular-material/material.module';
import { RouterModule } from '@angular/router';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    PdfViewerModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    LayoutComponent
  ],
  exports: [
    CommonModule
  ],
  entryComponents: [
  ]
})
export class SharedModule { }

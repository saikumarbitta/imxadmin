import { Component, ViewChild, OnInit, AfterViewInit, AfterContentInit } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { SharedService } from '../../services/shared.service';
import { ScreenSizeService } from '../../services/screen-size.service';
import { MatDialog } from '@angular/material';
import { CookieService } from 'ngx-cookie-service';
import { ScreenToggle } from '../../services/screen-toggle.service';
import { environment } from '../../../../environments/environment';
import { NGXLogger } from 'ngx-logger';
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, AfterViewInit,  AfterContentInit {

public dashboard1: string;


  title: string;
  RISK_URL: string;

  username = localStorage.getItem('currentUser');
  isriskanalyser = localStorage.getItem('isriskanalyser');

  isHandset: Observable<boolean> = this.screenSize.isHandset;

  constructor(
    private screenSize: ScreenSizeService,
    private router: Router,
    private shared: SharedService,
    private dialog: MatDialog,
    private cookieService: CookieService,
    public screenToggle: ScreenToggle,
    private logger: NGXLogger
  ) { }

  ngOnInit() {
    this.shared.title.subscribe(value => {
      this.title = value;
      if (this.title === 'Dashboard') {
        this.dashboard1 = 'home';
      }
      // console.log(this.title);
    });
    this.RISK_URL = environment.RISK_URL;
   // console.log(this.isriskanalyser);
    this.getRouterDetail();
  }

  ngAfterViewInit() {
    this.getRouterDetail();
  }

  ngAfterContentInit() {
    this.getRouterDetail();
  }
  getRouterDetail() {
    const temp = this.router.url;
    const temp1 = temp.split('/').filter((i) => i === 'home');
   // console.log( temp.split('/')[2]);

    // this.dashboard1= temp1[0];
    this.dashboard1 = temp.split('/')[2];
  }

  onClick(tabVal) {
    // console.log('tabVal');
// console.log(tabVal);
this.dashboard1 = tabVal;
  }
  logout() {
    this.screenToggle.exitFullscreen();
    this.shared.doctorId = null;
    this.cookieService.delete('currentUser');
    localStorage.removeItem('currentUser');

    this.cookieService.delete('isriskanalyser');
    localStorage.removeItem('isriskanalyser');
    // localStorage.clear();
    localStorage.removeItem('doc-app-doctor-id');
    localStorage.removeItem('totalRecords');
    localStorage.removeItem('api-payload');
    localStorage.removeItem('recordIndex');
    this.router.navigate(['']);
  }

  gotoImedrix() {
    window.open('http://www.imedrix.com', '_blank');
  }

  gotoRiskAnalyser() {
    window.open('http://localhost:4200/#app/dashboard', 'test');
    // console.log(localStorage.getItem('currentUser'));
    // console.log(localStorage.getItem('hispassword'));
  }

}

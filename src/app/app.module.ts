import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SigninAlertComponent } from './shared/components/signin-alert/signin-alert.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { AuthModule } from './auth/auth.module';
import { AppRoutingModule } from './app.routing-module';
import { PatientModule } from './patient/patient.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { CookieService } from 'ngx-cookie-service';
import { MaterialModule } from './angular-material/material.module';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireModule } from '@angular/fire';
// import { MAT_BOTTOM_SHEET_DEFAULT_OPTIONS } from '@angular/material';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetModule } from '@angular/material';
// import { PdfViewerModule } from 'ng2-pdf-viewer';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';
import { HttpClientModule } from '@angular/common/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

export class MyHammerConfig extends HammerGestureConfig {
  overrides = <any>{
    'swipe': { velocity: 0.4, threshold: 20 } // override default settings
  };
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    // PdfViewerModule,
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AuthModule,
    CoreModule,
    SharedModule,
    PatientModule,
    AppRoutingModule,
    MaterialModule,
    ServiceWorkerModule.register(
      '/i3doctordashboard/ngsw-worker.js',
      { enabled: environment.production }
    ),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase),
    LoggerModule.forRoot({
      serverLoggingUrl: `${environment.apiUrl}`,  //  full path to your api end-point for logging to server.
      level: environment.logLevel,    // TRACE, DEBUG, INFO, LOG, WARN, ERROR, FATAL and OFF
      serverLogLevel: environment.serverLogLevel, //  defines the minimum log level for server-side logging.
      disableConsoleLogging: false    //  flag which helps you to turn console logging completely off
    }),
    MatBottomSheetModule,
    NgMultiSelectDropDownModule
  ],
  providers: [{
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  }, {
    provide: HAMMER_GESTURE_CONFIG,
    useClass: MyHammerConfig
  },
  // {provide: MAT_BOTTOM_SHEET_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}},
  {provide: MAT_BOTTOM_SHEET_DATA, useValue: {hasBackdrop: false}},
    CookieService
  ],
  entryComponents: [
    SigninAlertComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

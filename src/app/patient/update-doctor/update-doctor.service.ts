import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UpdateDoctorService {

  private API_URL = environment.API_URL;
  private API_URL_COMMENT = environment.API_URL_COMMENT;

  constructor(private http: HttpClient) { }

  updateDoctor(type, doctor_id, doctor_name, speciality,
  mobile ,
  view, comment, forward, closure,
  doctor_notification, is_thirdparty_doctor, markas_delete): Observable<any> {

   const resetPasswordPayload = { type: type, doctor_id: doctor_id, doctor_name: doctor_name,
    speciality: speciality , doctor_phone: mobile,
    view:  view, comment: comment, forward: forward, closure: closure,
    notification_enabled: doctor_notification,  is_thirdparty_doctor: is_thirdparty_doctor,
    markas_delete: markas_delete};
  console.log(resetPasswordPayload);
  //  const body = new FormData();
  //  body.append('doctor_id', doctor_id);
  //  body.append('doctor_name', doctor_name);
  //  body.append('doctor_email', doctor_email);
  //  body.append('doctor_phone', doctor_mobile);
  //  body.append('speciality', doctor_speciality);
  //  body.append('disable', doctor_markas_delete);
  //  body.append('type', doctor_notification);
  //  body.append('notification_flag', doctor_userType);

   return this.http.post(
     this.API_URL + 'edit_app_users.php',
     resetPasswordPayload
     )
     .pipe(
       map(
         response => response
       )
     );
   }

  // updateOperator(operator_id, opname, opemail,
  //   opmobile, operator_type,  markas_delete,
  //   cloud_analyser_enabled, userType): Observable<any> {

  //  const resetPasswordPayload = { operator_id: operator_id, operator_name: opname,
  //                                  operator_email: opemail , operator_phone: opmobile,
  //                                  leads:  operator_type, disable: markas_delete,
  //                                  cloud: cloud_analyser_enabled,  type: userType};

  //  const body = new FormData();
  //  body.append('operator_id', operator_id);
  //  body.append('operator_name', opname);
  //  body.append('operator_email', opemail);
  //  body.append('operator_phone', opmobile);
  //  body.append('leads', operator_type);
  //  body.append('disable', markas_delete);
  //  body.append('cloud', cloud_analyser_enabled);
  //  body.append('type', userType);

  //  return this.http.post(
  //    this.API_URL + 'analytics/edit_appusers.php',
  //    body
  //    )
  //    .pipe(
  //      map(
  //        response => response
  //      )
  //    );
  //  }


  saveFeedback(payload): Observable<any> {
    return this.http.post(
      // this.API_URL + 'savefeedback', payload
      this.API_URL_COMMENT + 'log_mantis_i3.php', payload
    )
      .pipe(
        map(
          response => response
        )
      );
  }
}

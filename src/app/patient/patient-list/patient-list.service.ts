import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IPatientList, IOperators, IAppUsersList } from './patient-list.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { NGXLogger } from 'ngx-logger';
import { IHopsitalData } from '../../core/dashboard/dashboard.model';
// import { LoggerService } from '../../shared/services/logger.service';

@Injectable({
  providedIn: 'root'
})
export class PatientListService {

  private API_URL = environment.API_URL;
  private API_URL_COMMENT =  environment.API_URL_COMMENT;
  constructor(private http: HttpClient,
    private logger: NGXLogger,
    // public loggerService: LoggerService
  ) { }

  getFilterByValues() {
    const filterArray = [
      { value: '0', viewValue: 'None' },
      { value: 'Emergency', viewValue: 'Emergency' },
      { value: 'High', viewValue: 'High' },
      { value: 'Moderate', viewValue: 'Moderate' },
      { value: 'Low', viewValue: 'Low' }
    ];
    return filterArray;
  }

  getFilter2ByValues() {
    const filterArray2 = [
      { value: 'waiting', viewValue: 'Waiting' },
      { value: 'completed', viewValue: 'Completed' },
      { value: 'all', viewValue: 'All' }
    ];
    return filterArray2;
  }

  getOperators(doctor): Observable<IOperators> {
    // this.logger.info(this.loggerService.formateMsg('[Patient_List_Service]' , 43, 'getOperators()',
    //                               'Get operator for User : ' + doctor ));
    return this.http.post<IOperators>(
      this.API_URL + 'get.operator',
      { 'doctor_id': doctor }
    )
      .pipe(
        map(response => response['operators'])
      );
  }

  getHospitalDetails()  {
    return this.http.get(this.API_URL + 'get_hospital_details.php')
    .pipe(map(
      response => {
        return response;
      }
    ));
  }

  getAppUsers(selectedHospital): Observable<IAppUsersList> {
    const load = selectedHospital;
    return this.http.get<IAppUsersList>(
      this.API_URL + 'get_appusers.php?hospital_code=' + selectedHospital
    )
      .pipe(
        map(response => response)
      );
  }
  // getAppUsers(selectedHospital)  {
  //   return this.http.get(this.API_URL + 'analytics/get_appusers.php?hospital_code=' + selectedHospital)
  //   .pipe(map(
  //     response => {
  //       return response;
  //     }
  //   ));
  // }
}

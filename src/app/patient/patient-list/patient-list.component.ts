
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { MatPaginator, MatSort, MatTableDataSource, MatOptionSelectionChange, MatDialog, MatSnackBar } from '@angular/material';
import { PatientListService } from './patient-list.service';
import { IHopsitalData, IFormValues } from '../../core/dashboard/dashboard.model';
import { IOperators, IPatientDetails, IAppUsersList, IOperatorDetails, IDoctorDetails,  } from './patient-list.model';
import { SharedService } from '../../shared/services/shared.service';
import { Router, ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';


@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.scss']
})

export class PatientListComponent implements OnInit, OnDestroy {

  records: any;
  hosp_records = [];
  // hosp_code_records = [{ hosp_code: []}];
  hosp_code_records: any;
  data: any[];
  newdata = [];
  ndata: any;
  settings = {};
  noDataFound = false;

  patientlistForm: FormGroup;
  resetForm = 0;
  patientlistFormControl = new FormControl();
  filteredHospital: Observable<IHopsitalData[]>;
  filterFactors: { value: string, viewValue: string }[] = [];
  filterFactors2: { value: string, viewValue: string }[] = [];
  operators: IOperators;

  filterToDisplay: string;
  filterType: string;
  // private personSubsList: Subscription;
  totalRecords: any;
  // ismatpaginatorNotLoaded = true;

  isPatientListNotLoaded = true;

  isHospitalSelected = false;

  // patient list table
  displayedColumns: string[] = [
      'Name', 'Email', 'Phone', 'Leads', 'Cloud', 'Disabled', 'Mode', 'Sync', 'UserType',
      'Spot Check Monitor', 'Patient Name', 'Patient Age', 'Patient Mobile Number'
  ]; /* 'symptoms', , 'immediateattention' */
  displayedColumnsDoc: string[] = [
    'Name', 'Email', 'Phone', 'Speciality', 'Notification', 'Disabled', 'View', 'Comment',
    'Forward', 'Closure', 'ThirdpartyDoctor'
  ];
  is_thirdparty_doctor = localStorage.getItem('is_thirdparty_doctor');
  dataSource: MatTableDataSource<IOperatorDetails>;
  dataSourceDoc: MatTableDataSource<IDoctorDetails>;
  dataSourceExport: MatTableDataSource<IPatientDetails>;
  resultsLength = 0;
  manualPage = null;
  manualPageArray = [];
  tablepageNumber: number;

  listSubscription: Subscription;
  operatorSubscription: Subscription;

  hospitals: IHopsitalData[] = [];
  selectedHospital: string;
  selectedHospitalName: string;

  maxDate = new Date();

  searchByPhone = '';
  searchByName = '';

  sortOrder: any;

  selectedTab = 0;

  export_patient_list_payload: IFormValues ;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private plservice: PatientListService,
    private fb: FormBuilder,
    public dialog: MatDialog,
    private shared: SharedService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private snackbar: MatSnackBar
  ) {
  // console.log(this.activatedRoute.snapshot.params);
    this.filterToDisplay = this.activatedRoute.snapshot.params.filter;
    this.filterType = this.activatedRoute.snapshot.params.type;
   /* this.filterToDisplay = this.activatedRoute.snapshot.params.filter ?
   this.activatedRoute.snapshot.params.filter.charAt(0).toUpperCase() + this.activatedRoute.snapshot.params.filter.slice(1)
                            : this.activatedRoute.snapshot.params.filter;*/

  }



  ngOnInit() {

    this.settings = {
      singleSelection: true,
      idField: 'hospital_code',
      textField: 'hospital_name',
      enableCheckAll: true,
      // selectAllText: 'Select All',
      // unSelectAllText: 'Unselect All',
      allowSearchFilter: true,
      limitSelection: -1,
      clearSearchFilter: true,
      maxHeight: 197,
      itemsShowLimit: 3,
      searchPlaceholderText: 'Hospital Name',
      noDataAvailablePlaceholderText: 'No Hospital Found',
      closeDropDownOnSelection: true,
      showSelectedItemsAtTop: false,
      defaultOpen: false
    };

    // this.selectedTab = 0;
    // setting up page title
    this.shared.titleSource.next('Patient List');
    // console.log(this.filterToDisplay);
    // On Init Page Number is always 1 so
    this.tablepageNumber = 1;

    // form intialisation
    this.patientlistForm = this.fb.group({
      hospitalName: '',
      operatorId: '',
      dateRange: '',
      filter: '',
      filter2: this.getStaustFilterForThirdPartyDoctor(),
      searchText: '',
      searchPatientID: ''
    });
    const patient_list_payload: IFormValues = {
      'doctor_id': this.shared.doctorId,
      'page': 1,
      'isSearch':  this.is_thirdparty_doctor === '1'  ? '1' : this.filterToDisplay ? '1' : '0',
      'searchtxt': '',
      'searchPatientID': '',
      'phone': '',
      'hospital_code': this.activatedRoute.snapshot.params.hospital ? this.activatedRoute.snapshot.params.hospital : '',
      'oprator_id': '',
      'start_date': this.patientlistForm.value.dateRange[0] ?
                      this.getDate(new Date(this.patientlistForm.value.dateRange[0]).getTime()) : '',
        'end_date': this.patientlistForm.value.dateRange[1] ?
                      this.getDate(new Date(this.patientlistForm.value.dateRange[1]).getTime()) :
                      this.patientlistForm.value.dateRange[0] ? new Date() : '',
      'filter': this.filterToDisplay ? this.filterToDisplay.charAt(0).toUpperCase() + this.filterToDisplay.slice(1) : '',
      'sort': '',
      'export': 0,
      'status': this.getStaustFilterForThirdPartyDoctor()
      // 'status': this.is_thirdparty_doctor === '1'  ? 'waiting' : ''
    };

    // this.getDate(new Date().getTime() - 604800000),
    //     this.getDate(new Date().getTime())

    // get hodpital data
    this.getHospitalListAndPatientList(patient_list_payload);
    this.export_patient_list_payload = patient_list_payload;
    // get operators
    this.getOperators();

    // get filter values
    this.filterFactors = this.plservice.getFilterByValues();
    this.filterFactors2 = this.plservice.getFilter2ByValues();

    this.patientlistFormControl.valueChanges.subscribe(val => {
      if (val === '') {
        this.selectedHospital = '';
      }
    });

    this.patientlistForm.get('searchText').valueChanges.subscribe(val => {
      if (val === '') {
        // console.log(this.patientlistForm.value.dateRange);
        // console.log(this.resetForm );
        if (this.resetForm === 0) {
          // console.log('reset val');
          patient_list_payload.isSearch = this.is_thirdparty_doctor === '1'  ? '1' :
          this.filterToDisplay ? '1' :
          this.patientlistForm.value.dateRange[0] ? '1' : '0';

          patient_list_payload.start_date = this.patientlistForm.value.dateRange[0] ?
          this.getDate(new Date(this.patientlistForm.value.dateRange[0]).getTime()) : '';

          patient_list_payload.end_date = this.patientlistForm.value.dateRange[1] ?
          this.getDate(new Date(this.patientlistForm.value.dateRange[1]).getTime()) :
          this.patientlistForm.value.dateRange[0] ? new Date() : '';
        } else {
          patient_list_payload.isSearch =  '0';
        }


        this.getHospitalListAndPatientList(patient_list_payload);
        this.export_patient_list_payload = patient_list_payload;
        this.resetForm = 0;
      }
    });

    // this.patientlistForm.patchValue({
    //   dateRange: [
    //     new Date(new Date().getTime() - 604800000),
    //     new Date()
    //   ]
    //   // formControlName2: myValue2 (can be omitted)
    // });
  }

  getStaustFilterForThirdPartyDoctor() {
    return this.is_thirdparty_doctor === '1'  ?
         this.filterType === 'DC' || this.filterType === 'DCP' ? '' : 'waiting' : '';
  }

  onTabChanged(event) {
    // console.log(event);
    // console.log(event.index);
    this.selectedTab = event.index;
    this.shared.setSelectedTab(this.selectedTab);
  }

  addOperator() {
    if (this.selectedHospital !== undefined) {
      this.router.navigate(['hospital/add-operator/' + this.selectedHospital + '/' + this.selectedHospitalName]);
    } else {
      this.snackbar.open(
        'Please select the hospital to Continue',
        '',
        {
          duration: 3000
        }
      );
    }
  }

  addDoctor() {
    if (this.selectedHospital !== undefined) {
      this.router.navigate(['hospital/add-doctor/' + this.selectedHospital + '/' + this.selectedHospitalName]);
    } else {
      this.snackbar.open(
        'Please select the hospital to Continue',
        '',
        {
          duration: 3000
        }
      );
    }
  }

  getDate(unixTime) {
    const date = new Date(unixTime).toLocaleDateString().replace(/\//g, '-');
    const dateToSend = date.split('-');
    // return dateToSend[2] + '-' + dateToSend[1] + '-' + dateToSend[0];
    const date2 = new Date(unixTime);
    // console.log('Date');
    // console.log(date2);
    // console.log(formatDate(date2, 'yyyy-MM-dd hh:mm:ssZZZZZ', 'en_US'));
    // console.log(this.datepipe.transform(date2, 'dd/MM/yyyy'));
    // console.log(date2.toLocaleDateString().substring(0, 10) );
    // console.log(date2.toDateString().substring(0, 10) );
    // console.log(date2.toLocaleString().substring(0, 10) );
    // console.log(date2.toLocaleTimeString().substring(0, 10) );
    // console.log(date2.toTimeString().substring(0, 10) );
    // console.log(date2.toString().substring(0, 10) );
    // console.log(date2.toUTCString().substring(0, 10) );
    // console.log(date2.toISOString().substring(0, 10) );
    // return date2.toISOString().substring(0, 10) ;
    return formatDate(date2, 'yyyy-MM-dd', 'en_US');
  }

  selectedHospitalFunc(event: MatOptionSelectionChange, code , name) {
  //   if (event.isUserInput) {
  //     this.selectedHospital = code;
  //     this.selectedHospitalName = name;
  //   }
  //   this.isHospitalSelected = true;
  //  //  console.log(this.isHospitalSelected );
  //   this.plservice.getAppUsers(this.selectedHospital).subscribe(patienlist => {
  //     if (patienlist.Operators) {
  //           // this.isPatientListNotLoaded = false;
  //           // this.totalRecords = patienlist.pagination.totalrecord;
  //           // console.log('this.totalRecords :' + this.totalRecords);
  //           // fill table
  //           this.dataSource = new MatTableDataSource(patienlist.Operators);
  //     }
  //     if (patienlist.Doctors) {
  //       // this.isPatientListNotLoaded = false;
  //       // this.totalRecords = patienlist.pagination.totalrecord;
  //       // console.log('this.totalRecords :' + this.totalRecords);
  //       // fill table
  //       this.dataSourceDoc = new MatTableDataSource(patienlist.Doctors);
  // }
  //   });
  }

  getHospitalListAndPatientList(payload) {
    this.isPatientListNotLoaded = true;

    this.plservice.getHospitalDetails().subscribe(
      visitdetails => {
      //  console.log(visitdetails);
        this.records = visitdetails;
        this.newdata.push(this.records);
      //  console.log(this.records);
      //  console.log(this.newdata);
        if (this.records.length === 0) {
          this.noDataFound = true;
        }
      }
    );
  }

  getOperators() {

    // this.operatorSubscription = this.plservice.getOperators(this.shared.doctorId).subscribe(
    //   operators => {
    //     this.operators = operators;
    //   }
    // );
  }

  onSubmit(form: FormGroup) {
console.log(form);
    if (form.value.filter === '0') {
      this.filterToDisplay = '';
    } else {
      this.filterToDisplay = form.value.filter;
    }
    // set pag index to 0
    // this.paginator.pageIndex = 0;
    // clear goto page value
    this.clearManualPage();

    // prepare payload for submit with page 1 always
    const submitPayload: IFormValues = this.getPaginatePayload(1, '');

    // get list
    this.getHospitalListAndPatientList(submitPayload);
    this.export_patient_list_payload = submitPayload;
  }


  openComments(event) {
    if (event.operator_id) {
      console.log( event.role);
      this.shared.setOperator(event.operator_id , event.user_name, event.email_id,
                              event.mobile, event.ecg_type, event.cloud_enabled,
                            event.markas_delete,
                            event.mode, event.is_pdf_sync, event.sync,
                            event.loglevel, event.session_time, event.sample_freequency,
                            event.thermal_printer, event.user_usage_type, event.spot_moniter,
                            event.name_enabled, event.age_enabled, event.mobile_enabled,
                            event.reporting_required, event.QR_code_enabled,
                            event.country_code, event.hospital_code,
                            event.hospital_name, event.last_reset_password_time, event.terms_condition,
                            event.is_admin,
                            event.cityname, event.city_classification,
                            event.qualification, event.speciality,
                            event.role, event.region);
      this.router.navigate(['hospital/update-operator']);
    }
    if (event.doctor_id) {
      this.shared.setDoctor(event.doctor_id , event.firstname, event.emailid,
                            event.phone_no, event.speciality, event.notification_enabled,
                          event.markas_delete,
                          event.view, event.comment, event.forward,
                          event.closure, event.is_thirdparty_doctor);
        this.router.navigate(['hospital/update-doctor']);
    }
  }



  updateManualPage(index) {
    this.manualPage = index;
    // this.paginator.pageIndex = index - 1;
    this.paginate(index);
    this.tablepageNumber = index;
  }

  onPaginateChange(e) {
    // this.paginate(e.pageIndex + 1);
    // this.tablepageNumber = e.pageIndex + 1;
  }


  paginate(pageNumber) {
    const paginatePayload = this.getPaginatePayload(pageNumber, this.sortOrder ? this.sortOrder : '');
    this.getHospitalListAndPatientList(paginatePayload);
  }

  clearManualPage() {
    this.manualPage = null;
  }


  getPaginatePayload(pageNumber, sort) {
    let isSearch = '';

    const formvalue = this.patientlistForm.value;
    let pagepayload: IFormValues;
    if (this.selectedHospital ||
      formvalue.filter ||
      formvalue.operatorId ||
      formvalue.searchText ||
      formvalue.searchPatientID ||
      this.filterToDisplay ||
      formvalue.dateRange.length > 0 ||
      sort) {
      isSearch = '1';
    } else {
      isSearch = '0';
    }

    if (formvalue.filter) {
      if (formvalue.searchText) {
        const numbers = new RegExp(/^[0-9]+$/);
        if (numbers.test(formvalue.searchText)) {
            console.log('code is numbers 2');
            this.searchByName = '';
            this.searchByPhone = formvalue.searchText;
        } else {
            console.log('enter numbers only 2');
            this.searchByPhone = '';
            this.searchByName = formvalue.searchText;
        }
      }
      pagepayload = {
        'doctor_id': this.shared.doctorId,
        'page': pageNumber,
        'isSearch': this.is_thirdparty_doctor === '1' ? '1' : isSearch,
        // 'searchtxt': formvalue.searchText ? formvalue.searchText : '',
        'searchtxt': this.searchByName,
        'searchPatientID': formvalue.searchPatientID,
        'phone': this.searchByPhone,
        'hospital_code': this.selectedHospital ?
                          this.selectedHospital : this.activatedRoute.snapshot.params.hospital ?
                          this.activatedRoute.snapshot.params.hospital : '',
        'oprator_id': formvalue.operatorId ? formvalue.operatorId : '',
        'start_date': formvalue.dateRange[0] ? this.getDate(new Date(formvalue.dateRange[0]).getTime()) : '',
        'end_date': formvalue.dateRange[1] ?
                      this.getDate(new Date(formvalue.dateRange[1]).getTime()) :
                      formvalue.dateRange[0] ? new Date() : '',
        'filter': formvalue.filter === 0 ? '' : formvalue.filter,
        'sort': sort ? sort : '',
        'export': 0,
        'status':  this.is_thirdparty_doctor === '1' ? formvalue.filter2 : ''
      };
    } else {
      if (formvalue.searchText) {
        const numbers = new RegExp(/^[0-9]+$/);
        if (numbers.test(formvalue.searchText)) {
            console.log('code is numbers 2');
            this.searchByName = '';
            this.searchByPhone = formvalue.searchText;
        } else {
            console.log('enter numbers only 2');
            this.searchByPhone = '';
            this.searchByName = formvalue.searchText;
        }
      }
      pagepayload = {
        'doctor_id': this.shared.doctorId,
        'page': pageNumber,
        'isSearch':  this.is_thirdparty_doctor === '1' ? '1' : isSearch,
       // 'searchtxt': formvalue.searchText ? formvalue.searchText : '',
       'searchtxt': this.searchByName,
       'searchPatientID': formvalue.searchPatientID,
       'phone': this.searchByPhone,
        'hospital_code': this.selectedHospital ?
                          this.selectedHospital : this.activatedRoute.snapshot.params.hospital ?
                          this.activatedRoute.snapshot.params.hospital : '',
        'oprator_id': formvalue.operatorId ? formvalue.operatorId : '',
        'start_date': formvalue.dateRange[0] ? this.getDate(new Date(formvalue.dateRange[0]).getTime()) : '',
        'end_date': formvalue.dateRange[1] ?
                      this.getDate(new Date(formvalue.dateRange[1]).getTime()) :
                      formvalue.dateRange[0] ? new Date()  : '',
        'filter': this.activatedRoute.snapshot.params.filter ?
          this.activatedRoute.snapshot.params.filter.charAt(0).toUpperCase() + this.activatedRoute.snapshot.params.filter.slice(1)
          : '',
        'sort': sort ? sort : '',
        'export': 0,
        'status': this.is_thirdparty_doctor === '1'  ? formvalue.filter2 : ''
      };
    }



    return pagepayload;

  }

  public onFilterChange(item: any) {
    console.log(item);
  }
  public onDropDownClose(item: any) {
    // console.log(item);
  }

  public onItemSelect(item: any) {
    // console.log(item);
    this.selectedHospital = item.hospital_code;
      this.selectedHospitalName = item.hospital_name;

    this.isHospitalSelected = true;
    this.plservice.getAppUsers(this.selectedHospital).subscribe(patienlist => {
      if (patienlist.Operators) {
            this.dataSource = new MatTableDataSource(patienlist.Operators);
      }
      if (patienlist.Doctors) {
        this.dataSourceDoc = new MatTableDataSource(patienlist.Doctors);
  }
    });
  }
  public onDeSelect(item: any) {
    console.log('route');
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate(['\hospital']);
    // this.router.routeReuseStrategy.shouldReuseRoute = function () {
    //   return false;
    // };
    // this.router.navigate(['hospital']);
  }

  public onSelectAll(items: any) {
    console.log(items);
  }
  public onDeSelectAll(items: any) {
    console.log(items);
  }

  sortData(event) {
    // this.manualPageArray = [];
    this.updateManualPage(1);
    this.sortOrder = event;
    const x = this.getPaginatePayload(1, event);
    x.isSearch = '1';
    this.getHospitalListAndPatientList(x);
  }

  ngOnDestroy(): void {
    if (this.listSubscription) { this.listSubscription.unsubscribe(); }
    if (this.operatorSubscription) { this.operatorSubscription.unsubscribe(); }
    localStorage.removeItem('pageTitle');
  }

}


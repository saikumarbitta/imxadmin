export interface IAppUsersList {
    Operators: IOperatorDetails[];
    Doctors: IDoctorDetails[];
    pagination: IPagination;
}

export interface IDoctorDetails {
    doctor_id: string;
    emailid: string;
    firstname: string;
    lastname: string;
    markas_delete: string;
    notification_flag: string;
    order_ref_no: string;
    password: string;
    phone_no: string;
    speciality: string;
}

export interface IOperatorDetails {
    auth_token: string;
    auth_token_updated: string;
    cloud_analyser_enabled: string;
    emailid: string;
    hospital_code: string;
    is_admin: string;
    markas_delete: string;
    mobile: string;
    name: string;
    operator_id: string;
    operator_type: string;
    order_ref_no: string;
    password: string;
    user_cloud_enabled: string;
}

export interface IPatientList {
    data: IPatientDetails[];
    doctorDetails: string;
    pagination: IPagination;
}


export interface IPatientDetails {
    age: string;
    commented: string;
    details: any;
    doctorname: string;
    emailid: string;
    emergency_rate: string;
    filepath: string;
    gender: string;
    hospital_code: string;
    hospital_name: string;
    name: string;
    operator_id: string;
    operator_name: string;
    patient_id: string;
    status: string;
    symptoms: any[];
    tagged: string;
    total: string;
    uhid: string;
    uncommented: string;
    visitID: string;
}

export interface IPagination {
    end: number;
    nextp: number;
    prep: number;
    start: number;
    totalrecord: number;
}


export interface IOperators {
    operator_id: string;
    operator_name: string;
}



export interface IFamilyDetails {
    stroke: string;
    ckd: string;
    others: string;
    cvd_events: string;
    pathological_hypertension: string;
    pathological_diabetes_mellitus: string;
}

export interface Icomments {
  doctor_name: string;
  comments: string;
}


export interface IBMIdetails {
    relative_risk: string;
    baseline_risk: string;
    heart_age: string;
    imedrix_score: string;
    symptoms: any[];
    symptomsID: any[];

    weight: string;
    height: string;
    bmi_value: string;
    arterial_mean_systolic: string;
    arterial_mean_diastolic: string;
    RBS: string;
    SPO2: string;
    temperature: string;
  respiratoryRate: string;
  pulse: string;

  HTN_Classification: string;
  DM_Classification: string;
}

// export interface IVitalDetails {
 // weight: string;
  // height: string;
  // bmi_value: string;
  // arterial_mean_systolic: string;
  // arterial_mean_diastolic: string;
  // RBS: string;
  // oxygen_saturation: string;
  // temperature: string;
  // respiratoryRate: string;
  // pulse: string;
// }

export interface IPathologicalDetails {
    smoking_status: string;
    alcohol_status: string;
    family_hypertensive: string;
    family_diabeties_mellitus: string;
    cholesterol: string;
    excerise: string;
    kidney_diseases: string;
    steroid_medication: string;
    lupus: string;
    previous_cardio_Abnormality: string;
    other_pathological_information: string;
}

export interface IPatientListDetails {

    UHID: string;
    patient_name: string;
    age: number;
    martialstatus: string;
    email_id: any;
    address: string;
    hospital_code: string;
    filepath: string;
    gender: string;
    mobile: number;
    location: string;
    location_other: string;
    ssn: string;
    country: string;
    city: string;
    bmi_details: IBMIdetails;
    // vital_details: IVitalDetails;
    pathological_details: IPathologicalDetails;
    family_details: IFamilyDetails;
    comments: Icomments;
    emergency_rate: number;
    doctor_id: string;
}

export interface IUserLocation {
    country_code: string;
    country_name: string;
    city: string;
    postal: string;
    latitude: number;
    longitude: number;
    IPv4: string;
    state: string;
}

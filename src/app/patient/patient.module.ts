import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PatientRoutingModule } from './patient-routing.module';
import { PatientListComponent } from './patient-list/patient-list.component';
import { SharedModule } from '../shared/shared.module';
import { MaterialModule } from '../angular-material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { CalendarModule } from 'primeng/calendar';
import {MatTabsModule} from '@angular/material/tabs';
import { UpdateOperatorComponent } from './update-operator/update-operator.component';
import { UpdateDoctorComponent } from './update-doctor/update-doctor.component';
import { AddOperatorComponent } from './add-operator/add-operator.component';
import { AddDoctorComponent } from './add-doctor/add-doctor.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    PatientRoutingModule,
    PdfViewerModule,
    CalendarModule,
    MatTabsModule,
    NgMultiSelectDropDownModule
  ],
  declarations: [
    PatientListComponent,
    UpdateOperatorComponent,
    UpdateDoctorComponent,
    AddOperatorComponent,
    AddDoctorComponent
  ],
  entryComponents: [
  ]
})
export class PatientModule { }

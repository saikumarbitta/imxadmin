import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AddDoctorService } from './add-doctor.service';
import { MatSnackBar, MatBottomSheetRef, MAT_DIALOG_DATA, MatDialogRef, MatBottomSheet } from '@angular/material';
import { SharedService } from '../../shared/services/shared.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-add-doctor',
  templateUrl: './add-doctor.component.html',
  styleUrls: ['./add-doctor.component.scss']
})
export class AddDoctorComponent implements OnInit {

  doctor_id: any;
  doctor_name: any;
  doctor_lastname: any;
  doctor_emailid: any;
  pass: any;
  doctor_mobile: any;
  doctor_speciality = '';
  doctor_notification: any;
  doctor_markas_delete: any;

  view: any;
  comment: any;
  forward: any;
  closure: any;
  is_thirdparty_doctor: any;

  changed_doctor_mobile: any;
  changed_doctor_speciality: any;
  changed_doctor_notification: any;
  changed_doctor_markas_delete: any;

  changed_view: any;
  changed_comment: any;
  changed_forward: any;
  changed_closure: any;
  changed_is_thirdparty_doctor: any;

  title = '';
  usertype = '';

  defaultZERO = '0';
  defaultONE = '1';

  defaultnotific = '0';
  defaultDisabled = '0';
  default3rdParty = '0';
  defaultview = '0';
  defaultcomment = '0';
  defaultforward = '0';
  defaultclose = '0';


  feedbackform: FormGroup;
  routerSubscription: Subscription;
  isFeedbackLoaded = false;
  selectedFile: File;

  imageError = '';
  fileName = '';
    isImageSaved: boolean;
    cardImageBase64: string;
  base64textString: string;

  hospCode: any;
  hospName: any;

  constructor(
    private fb: FormBuilder,
     private fbservice: AddDoctorService,
      private snackbar: MatSnackBar,
      private router: Router,
      private shared: SharedService,
      private activatedRoute: ActivatedRoute,
      private _bottomSheet: MatBottomSheet) {

        if (router.url.indexOf('page') === -1) {
          this.routerSubscription = router.events.pipe(
            filter(event => event instanceof NavigationEnd)
          ).subscribe((event: NavigationEnd) => {
            this.shared.titleSource.next('Feedback');
          });
        }

       }

  ngOnInit() {
    const selectedeDoctor = this.shared.getSelectedeDoctor();
    this.hospCode = this.activatedRoute.snapshot.params.code;
    this.hospName = this.activatedRoute.snapshot.params.hospital;

    this.changed_doctor_speciality =  this.defaultZERO  ;
    this.changed_doctor_markas_delete = this.defaultZERO ;
    this.changed_doctor_notification = this.defaultZERO ;

    this.changed_view =  this.defaultZERO ;
      this.changed_comment =  this.defaultZERO ;
      this.changed_forward =  this.defaultZERO ;
      this.changed_closure =  this.defaultZERO ;
      this.changed_is_thirdparty_doctor =  this.defaultZERO ;

    // if (this.shared.getSelectedTab() === 1) {
        this.title = 'Add Doctor';

        // this.setDoctor(selectedeDoctor.doctor_id , selectedeDoctor.doctor_name, selectedeDoctor.doctor_emailid,
        //   selectedeDoctor.doctor_mobile, selectedeDoctor.speciality, selectedeDoctor.notification_flag,
        //   selectedeDoctor.doctor_markas_delete,
        //   selectedeDoctor.view, selectedeDoctor.comment, selectedeDoctor.forward,
        //   selectedeDoctor.closure, selectedeDoctor.is_thirdparty_doctor);
    // }

    this.feedbackform = this.fb.group({

      docname: ['', Validators.required],
      doclastname: [''],
      docemail: ['', Validators.required],
      oppass: ['', Validators.required],
      docmobile: ['', Validators.required],
      dcspeciality: [''],
      dcnotification: [''],
      docdisabled: [''],
      ophospital_code: [''],
      ophospital_name: ['']
    });

  }

setDoctor(doctor_id , doctor_name, doctor_emailid,
  doctor_mobile, speciality, notification_flag, doctor_markas_delete,
  view, comment, forward, closure, is_thirdparty_doctor) {
    this.doctor_id = doctor_id;
    this.doctor_name = doctor_name;
    this.doctor_emailid = doctor_emailid;
    this.doctor_mobile = doctor_mobile;
    this.doctor_speciality = speciality;
    this.doctor_notification =   notification_flag ;
    this.doctor_markas_delete = new FormControl(doctor_markas_delete);

    this.view = new FormControl(view);
      this.comment = new FormControl(comment);
      this.forward = new FormControl(forward);
      this.closure = new FormControl(closure);
      this.is_thirdparty_doctor = new FormControl(is_thirdparty_doctor);

}
change_view  (value) {
  this.changed_view = value;
}
change_comment (value) {
  this.changed_comment = value;
}
change_forward (value) {
  this.changed_forward = value;
}
change_closure (value) {
  this.changed_closure = value;
}

change_is_thirdparty_doctor (value) {
  this.changed_is_thirdparty_doctor = value;
}

change_doctor_notification (value) {
  this.changed_doctor_notification  = value;
}

change_doctor_markas_delete (value) {
  this.change_doctor_markas_delete  = value;
}


OnInput(event: any) {
  // this.changedopmobile = event.target.value;
  }

  sendFeedBack(form: FormGroup) {
    // if (this.shared.getSelectedTab() === 1) {
      this.fbservice.addDoctor('doctor', this.hospCode, form.value.docemail,
      form.value.docname, form.value.doclastname, form.value.oppass,
      form.value.dcspeciality === '' || undefined ? this.doctor_speciality : form.value.dcspeciality,
        form.value.docmobile === '' ? this.doctor_mobile : form.value.docmobile ,
        this.changed_view, this.changed_comment, this.changed_forward, this.changed_closure,
        this.changed_doctor_notification, this.changed_is_thirdparty_doctor)
        .subscribe( response => {
          if (response['Result'] === 'Success') {
            this.snackbar.open(
              this.doctor_name + ' updated successfully!',
              '',
              {
                duration: 3000
              }
            );
            // this.dialogref.close();
            this.router.navigate(['hospital']);
          } else {
            this.snackbar.open(
              'Error' + response.message,
              '',
              {
                duration: 3000
              }
            );
            // this.dialogref.close();
          }
        });
    // }
  }

close() {
  this.router.navigate(['hospital']);
}

}




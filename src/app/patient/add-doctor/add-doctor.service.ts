import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AddDoctorService {

  private API_URL = environment.API_URL;
  private API_URL_COMMENT = environment.API_URL_COMMENT;

  constructor(private http: HttpClient) { }

  addDoctor(type, hospCode, emailid,
  name, lastname, pass, speciality,
    mobile , view, comment, forward, closure,
    notification, is_thirdparty_doctor): Observable<any> {

   const resetPasswordPayload = { type: type, hospital_code: hospCode,
    doctor_email: emailid , doctor_name: name, lastname: lastname,
    password:  pass, speciality: speciality, doctor_phone: mobile,
    view: view,  comment: comment, forward: forward, closure: closure,
    notification_enabled: notification, is_thirdparty_doctor: is_thirdparty_doctor};

  //  const body = new FormData();
  //  body.append('doctor_id', doctor_id);
  //  body.append('doctor_name', doctor_name);
  //  body.append('doctor_email', doctor_email);
  //  body.append('doctor_phone', doctor_mobile);
  //  body.append('speciality', doctor_speciality);
  //  body.append('disable', doctor_markas_delete);
  //  body.append('type', doctor_notification);
  //  body.append('notification_flag', doctor_userType);
  console.log(resetPasswordPayload);
   return this.http.post(
     this.API_URL + 'add_app_users.php',
     resetPasswordPayload
     )
     .pipe(
       map(
         response => response
       )
     );
   }


}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PatientListComponent } from './patient-list/patient-list.component';
import { LayoutComponent } from '../shared/components/layout/layout.component';
import { AuthGuard } from '../auth/auth-guard.service';
import { UpdateOperatorComponent } from './update-operator/update-operator.component';
import { UpdateDoctorComponent } from './update-doctor/update-doctor.component';
import { AddOperatorComponent } from './add-operator/add-operator.component';
import { AddDoctorComponent } from './add-doctor/add-doctor.component';

const routes: Routes = [
  {
    path: 'hospital', component:  LayoutComponent, canActivateChild: [AuthGuard], children: [
      { path: '', redirectTo: 'hospital-list', pathMatch: 'full' },
      { path: 'hospital-list', component: PatientListComponent},
      { path: 'hospital-list/:filter', component: PatientListComponent},
      { path: 'hospital-list/:filter/:type/:hospital', component: PatientListComponent},
      { path: 'hospital-list/:filter/:hospital', component: PatientListComponent},
      { path: 'add-operator/:code/:hospital', component: AddOperatorComponent},
      { path: 'add-doctor/:code/:hospital', component: AddDoctorComponent},
      { path: 'update-operator', component: UpdateOperatorComponent},
      { path: 'update-doctor', component: UpdateDoctorComponent},
      { path: '**', redirectTo: 'hospital-list', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientRoutingModule { }

import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { UpdateOperatorService } from './update-operator.service';
import { MatSnackBar, MatBottomSheetRef, MAT_DIALOG_DATA, MatDialogRef, MatBottomSheet } from '@angular/material';
import { SharedService } from '../../shared/services/shared.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-update-operator',
  templateUrl: './update-operator.component.html',
  styleUrls: ['./update-operator.component.scss']
})
export class UpdateOperatorComponent implements OnInit {


  operator_id: any;
  name: any;
  emailid: any;
  mobile: any;
  operator_type: any;
  cloud_analyser_enabled: any;
  markas_delete: any;
  mode: any;
  pdfDenoiseSync: any;
  sync: any;
  logLevel: any;
  sesTimeout: any;
  sampFreq: any;
  tPrinter: any;
  userType: any;
  spotMonitor: any;
  patientName: any;
  patientAge: any;
  patientPhone: any;
  ecgRep: any;
  qrCode: any;
  country_code: any;
  hospital_code: any;
  hospital_name: any;
  last_reset_password_time: any;
  terms_condition: any;
  is_Admin: any;
  cityname: any;

  city_classification: any;
  qualification: any;
  speciality: any;
  role: any;
  region: any;

  changed_mobile: any;
  changed_operator_type: any;
  changed_markas_delete: any;
  changed_cloud_analyser_enabled: any;

  changed_mode: any;
  changed_pdfDenoiseSync: any;
  changed_sync: any;
  changed_logLevel: any;
  changed_sesTimeout: any;
  changed_sampFreq: any;
  changed_tPrinter: any;
  changed_userType: any;
  changed_spotMonitor: any;
  changed_patientName: any;
  changed_patientAge: any;
  changed_patientPhone: any;
  changed_ecgRep: any;
  changed_qrCode: any;

  changed_country_code: any;
  changed_hospital_code: any;
  changed_hospital_name: any;
  changed_last_reset_password_time: any;
  changed_terms_condition: any;
changed_is_Admin: any;


changed_cityname: any;
changed_city_classification: any;
changed_qualification: any;
changed_speciality: any;
changed_role: any;
changed_region: any;


  title = '';
  usertype = '';



  qualificationList: string[] = [
    'MBBS',
    'MD',
    'MBBS, MD',
    'BAMS',
    'DM',
    'BHMS',
    'BUMS',
    'MD, MRCP',
    'MBBS DGO',
    'MBA',
    'MBBS, DNB',
    'RMP',
    'MD DNB',
    'MBBS, MS',
    'MS',
    'DMLT',
    'MBBS,DM',
    'MD, IDCC',
    'ICU Technician',
    'MD DM',
    'MBBS ,DNB',
    'MBBS, DIP CARD',
    'DNB,CTVS',
    'DHMS'
  ];

  specialityList: string[] = [
    'Cardiologist',
    'Consultant Physician',
    'Paediatrics',
    'General Practitioner-Ayurveda',
    'General Practitioner-Homeopathic',
    'Marketing',
    'General Practitioner',
    'Unani Medicine and Surgery',
    'Hospital Super',
    'Medical Officer',
    'General Physician',
    'Rural medical practitioner',
    'General Physician & Surgery',
    'General Physician & Master of Surgery',
    'Master of Surgery',
    'ICU Technician',
    'Diabetology',
    'General Practitioner-Unani',
    'Pulmonologist ',
    'Diploma in medical Laboratory',
    'Cardiologist & Cardio thoracic &Vascular Surg'
  ];

  feedbackform: FormGroup;
  routerSubscription: Subscription;
  isFeedbackLoaded = false;
  selectedFile: File;

  imageError = '';
  fileName = '';
    isImageSaved: boolean;
    cardImageBase64: string;
  base64textString: string;

  constructor(
    private fb: FormBuilder,
     private fbservice: UpdateOperatorService,
      private snackbar: MatSnackBar,
      private router: Router,
      private shared: SharedService,
      private activatedRoute: ActivatedRoute,
      private _bottomSheet: MatBottomSheet) {

        if (router.url.indexOf('page') === -1) {
          this.routerSubscription = router.events.pipe(
            filter(event => event instanceof NavigationEnd)
          ).subscribe((event: NavigationEnd) => {
            this.shared.titleSource.next('Feedback');
          });
        }

       }

  ngOnInit() {
    const selectedOperator =  this.shared.getSelectedOperator();
    const selectedeDoctor = this.shared.getSelectedeDoctor();

    if (this.shared.getSelectedTab() === 0) {
      this.title = selectedOperator.name + '(' + selectedOperator.operator_id + ')';
      this.usertype = 'Operator ';
// console.log(selectedOperator.cloud_analyser_enabled);
      this.setOperator(selectedOperator.operator_id , selectedOperator.name, selectedOperator.emailid,
        selectedOperator.mobile, selectedOperator.operator_type, selectedOperator.cloud_analyser_enabled,
        selectedOperator.markas_delete, selectedOperator.mode, selectedOperator.pdfDenoiseSync, selectedOperator.sync,
        selectedOperator.logLevel, selectedOperator.sesTimeout,  selectedOperator.sampFreq, selectedOperator.tPrinter,
        selectedOperator.userType, selectedOperator.spotMonitor, selectedOperator.patientName, selectedOperator.patientAge,
        selectedOperator.patientPhone, selectedOperator.ecgRep, selectedOperator.qrCode,
        selectedOperator.countrycode, selectedOperator.hospital_code,
        selectedOperator.hospital_name, selectedOperator.last_reset_password_time, selectedOperator.terms_condition,
        selectedOperator.is_admin, selectedOperator.cityname,
        selectedOperator.city_classification, selectedOperator.qualification,
        selectedOperator.speciality, selectedOperator.role, selectedOperator.region
      );
    }

    this.feedbackform = this.fb.group({
      // confirmPassword: ['', Validators.required],
      opname: [''],
      opemail: [''],
      opmobile: [''],
      // leads: this.operator_type,
      // clouds: ['', Validators.required],
      // opdisabled: ['', Validators.required],
      opsesTimeout: [''],
      opcountry_code: [''],
      ophospital_code: [''],
      ophospital_name: [''],
      oplast_reset_password_time: [''],
      opterms_condition: [''],
      opcityname: [''],
    });

  }

  setOperator(operator_id , name, emailid,
    mobile, operator_type, cloud_analyser_enabled,
    markas_delete,
    mode, pdfDenoiseSync, sync,
    logLevel, sesTimeout, sampFreq,
    tPrinter, userType, spotMonitor,
    patientName, patientAge, patientPhone,
    ecgRep, qrCode,
    country_code, hospital_code,
    hospital_name, last_reset_password_time, terms_condition,
    is_Admin, cityname,
    city_classification, qualification,
    speciality, role, region) {
      this.operator_id = operator_id;
      this.name = name;
      this.emailid = emailid;
      this.mobile = mobile;
      this.operator_type = new FormControl(operator_type);
      this.cloud_analyser_enabled = new FormControl(cloud_analyser_enabled);
      this.markas_delete = new FormControl(markas_delete);

      this.mode = new FormControl(mode);
    this.pdfDenoiseSync = new FormControl(pdfDenoiseSync);
    this.sync = new FormControl(sync);
    this.logLevel = new FormControl(logLevel);
    this.sesTimeout = sesTimeout;

    this.sampFreq = new FormControl(sampFreq);
    this.tPrinter = new FormControl(tPrinter);
    this.userType = new FormControl(userType);
    this.spotMonitor = new FormControl(spotMonitor);
    this.patientName = new FormControl(patientName);

    this.patientAge = new FormControl(patientAge);
    this.patientPhone = new FormControl(patientPhone);
    this.ecgRep = new FormControl(ecgRep);
    this.qrCode = new FormControl(qrCode);

    this.country_code = country_code;
    this.hospital_code = hospital_code;
    this.hospital_name = hospital_name;
    this.last_reset_password_time = last_reset_password_time;
    this.terms_condition = terms_condition;

    this.is_Admin = new FormControl(is_Admin);
    this.cityname = cityname;

    this.city_classification =  new FormControl(city_classification);
    this.qualification = new FormControl(qualification);
    this.speciality = new FormControl(speciality);
    this.role = new FormControl(role);
    this.region = new FormControl(region);

      this.changed_mobile = mobile;
      this.changed_operator_type = operator_type ;
      this.changed_markas_delete = markas_delete ;
      this.changed_cloud_analyser_enabled = cloud_analyser_enabled ;

      this.changed_mode = mode ;
      this.changed_pdfDenoiseSync = pdfDenoiseSync ;
      this.changed_sync = sync ;
      this.changed_logLevel = logLevel ;
      this.changed_sesTimeout = sesTimeout ;

      this.changed_sampFreq = sampFreq ;
      this.changed_tPrinter = tPrinter ;
      this.changed_userType = userType ;
      this.changed_spotMonitor = spotMonitor ;
      this.changed_patientName = patientName ;

      this.changed_patientAge = patientAge ;
      this.changed_patientPhone = patientPhone ;
      this.changed_ecgRep = ecgRep ;
      this.changed_qrCode = qrCode  ;

      this.changed_country_code = country_code;
    this.changed_hospital_code = hospital_code;
    this.changed_hospital_name = hospital_name;
    this.changed_last_reset_password_time = last_reset_password_time;
    this.changed_terms_condition = terms_condition;

    this.changed_is_Admin = is_Admin  ;
  this.changed_cityname = cityname;
  this.changed_city_classification = city_classification;
  this.changed_qualification = qualification;
  this.changed_speciality = speciality;
  this.changed_role = role;
  this.changed_region = region;
}

change_is_admin(value) {
  this.changed_is_Admin = value;
}

change_qrCode (value) {
  this.changed_qrCode = value;
}

change_ecgRep (value) {
  this.changed_ecgRep = value;
}

change_patientAge  (value) {
  this.changed_patientAge = value;
}

change_patientName (value) {
  this.changed_patientName = value;
}

change_spotMonitor (value) {
  this.changed_spotMonitor = value;
}
change_userType (value) {
  this.changed_userType = value;
}

change_tPrinter (value) {
  this.changed_tPrinter = value;
}

change_sampFreq (value) {
  this.changed_sampFreq = value;
}
change_sesTimeout (value) {
  this.changed_sesTimeout = value;
}
change_logLevel(value) {
  this.changed_logLevel = value;
}

change_sync (value) {
  this.changed_sync = value;
}

change_pdf (value) {
  this.changed_pdfDenoiseSync = value;
  console.log(value);
}

change_mode (value) {
  this.changed_mode  = value;
}

change_mob (value) {
  this.changed_mobile  = value;
}

change_op_type (value) {
  // console.log(value);
  this.changed_operator_type = value;
}

change_op_markas_delete (value) {
  // console.log(value);
  this.changed_markas_delete = value;
}
change_patientPhone (value) {
}
change_op_cloud_analyser_enabled (value) {
  // console.log(value);
  this.changed_cloud_analyser_enabled = value;
}

change_cityname (value) {
  this.changed_cityname  = value;
}

change_city_classification (value) {
  this.changed_city_classification  = value;
}
change_qualification (value) {
  this.changed_qualification  = value;
}
change_speciality (value) {
  this.changed_speciality  = value;
}
change_role (value) {
  this.changed_role  = value;
}
change_region (value) {
  this.changed_region  = value;
}
OnInput(event: any) {
  // this.changedopmobile = event.target.value;
  }

  sendFeedBack(form: FormGroup) {

    // console.log(this.changed_qualification);
    // if (this.shared.getSelectedTab() === 0) {
      this.fbservice.updateOperator('operator', this.operator_id, form.value.opmobile === '' ? this.mobile : form.value.opmobile ,
      this.changed_cloud_analyser_enabled, this.changed_operator_type, this.is_Admin,
      this.sampFreq, this.logLevel, this.sync, form.value.opsesTimeout === '' ? this.sesTimeout : form.value.opsesTimeout ,
      this.tPrinter, this.userType,
      this.spotMonitor, this.patientName, this.patientAge, this.patientPhone, this.qrCode,
      this.mode, this.changed_pdfDenoiseSync, this.changed_markas_delete , this.changed_ecgRep,
      form.value.opcityname === '' ? this.cityname : form.value.opcityname,
      this.changed_city_classification, this.changed_qualification,
      this.changed_speciality, this.changed_role, this.changed_region)
        .subscribe( response => {
          console.log(response);
          if (response['Result'] === 'Success') {
            this.snackbar.open(
              this.name + ' updated successfully!',
              '',
              {
                duration: 3000
              }
            );
            // this.dialogref.close();
            this.router.navigate(['hospital']);
          } else {
            this.snackbar.open(
              'Error ' + response.Response,
              '',
              {
                duration: 3000
              }
            );
            // this.dialogref.close();
          }
        });
    // }
  }

close() {
  this.router.navigate(['hospital']);
}

}




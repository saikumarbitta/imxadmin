import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UpdateOperatorService {

  private API_URL = environment.API_URL;
  private API_URL_COMMENT = environment.API_URL_COMMENT;

  constructor(private http: HttpClient) { }

  // updateDoctor(doctor_id, doctor_name, doctor_email,
  //   doctor_mobile, doctor_speciality,  doctor_markas_delete,
  //   doctor_notification, doctor_userType): Observable<any> {

  //  const body = new FormData();
  //  body.append('doctor_id', doctor_id);
  //  body.append('doctor_name', doctor_name);
  //  body.append('doctor_email', doctor_email);
  //  body.append('doctor_phone', doctor_mobile);
  //  body.append('speciality', doctor_speciality);
  //  body.append('disable', doctor_markas_delete);
  //  body.append('type', doctor_notification);
  //  body.append('notification_flag', doctor_userType);

  //  return this.http.post(
  //    this.API_URL + 'analytics/edit_doctordetails.php',
  //    body
  //    )
  //    .pipe(
  //      map(
  //        response => response
  //      )
  //    );
  //  }

  updateOperator(useropType, operator_id, opmobile,
    cloud_analyser_enabled, operator_type, is_Admin,
    sampFreq, logLevel, sync, sesTimeout, tPrinter, usertype,
    spotMonitor, patientName, patientAge, patientPhone, qrCode,
    mode, pdfDenoiseSync, markas_delete, ecgRep, cityname,
    city_classification, qualification,
    speciality, role, region): Observable<any> {

      console.log(region);
      console.log(role);

   const resetPasswordPayload = { type: useropType, operator_id: operator_id, mobile: opmobile,
                                  cloud_enabled: cloud_analyser_enabled === undefined ? 'N/A' : cloud_analyser_enabled,
                                  ecg_type:  operator_type ,
                                  is_admin: is_Admin === undefined ? 'N/A' : is_Admin.value ,
                                  sample_freequency: sampFreq  === undefined ? 'N/A' : sampFreq.value,
                                  loglevel: logLevel  === undefined ? 'N/A' : logLevel.value,
                                  sync: sync === undefined ? 'N/A' : sync.value === undefined ? 'N/A' :  sync.value,
                                  session_time: sesTimeout,
                                  thermal_printer: tPrinter === undefined ? 'N/A' : tPrinter.value,
                                  user_usage_type: usertype === undefined ? 'N/A' : usertype.value,
                                  spot_moniter: spotMonitor === undefined ? 'N/A' : spotMonitor.value,
                                  name_enabled: patientName === undefined ? 'N/A' : patientName.value,
                                  age_enabled: patientAge === undefined ? 'N/A' : patientAge.value,
                                  mobile_enabled: patientPhone === undefined ? 'N/A' : patientPhone.value,
                                  QR_code_enabled: qrCode === undefined ? 'N/A' : qrCode.value,
                                  mode: mode === undefined ? 'N/A' : mode.value,
                                  is_pdf_sync: pdfDenoiseSync === undefined ? 'N/A' : pdfDenoiseSync,
                                  mark_as_delete: markas_delete === undefined ? 'N/A' : markas_delete,
                                  reporting_required: ecgRep === undefined ? 'N/A' : ecgRep,
                                  cityname: cityname === undefined ? 'N/A' : cityname,
                                  city_classification: city_classification === undefined ? 'N/A' : city_classification,
                                  qualification: qualification === undefined ? 'N/A' : qualification,
                                  speciality: speciality === undefined ? 'N/A' : speciality,
                                  role: role === undefined ? 'N/A' : role,
                                  region: region === undefined ? 'N/A' : region
                                  };

  //  const body = new FormData();
  //  body.append('operator_id', operator_id);
  //  body.append('operator_name', opname);
  //  body.append('operator_email', opemail);
  //  body.append('operator_phone', opmobile);
  //  body.append('leads', operator_type);
  //  body.append('disable', markas_delete);
  //  body.append('cloud', cloud_analyser_enabled);
  //  body.append('type', userType);
console.log (resetPasswordPayload);
   return this.http.post(
     this.API_URL + 'edit_app_users.php',
     resetPasswordPayload
     )
     .pipe(
       map(
         response => response
       )
     );
   }

extractValue(field, getField, getVal) {
  if (field.value !== undefined && getVal) {
    return field.value;
  }
  if (field !== undefined && getField) {
    return field;
  }

  return 'N/A';
}
  saveFeedback(payload): Observable<any> {
    return this.http.post(
      // this.API_URL + 'savefeedback', payload
      this.API_URL_COMMENT + 'log_mantis_i3.php', payload
    )
      .pipe(
        map(
          response => response
        )
      );
  }
}

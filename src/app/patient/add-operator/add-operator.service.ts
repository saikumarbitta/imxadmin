import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AddOperatorService {

  private API_URL = environment.API_URL;
  private API_URL_COMMENT = environment.API_URL_COMMENT;

  constructor(private http: HttpClient) { }

  addOperator(type, hospCode, opemail, opname,
  opmobile ,  oppass, cloud_analyser_enabled, changed_operator_type,
  changed_is_admin, changed_sampFreq, changed_logLevel, changed_sync,
  changed_sesTimeout, changed_tPrinter, changed_userType,
  changed_spotMonitor,
  changed_patientName, changed_patientAge, changed_patientPhone, changed_qrCode,
  changed_mode, changed_pdfDenoiseSync, countrycode,
  changed_qualification, city_name, speciality, city_classification,
  changed_role, changed_region): Observable<any> {

   const resetPasswordPayload = { type: type, hospital_code: hospCode, emailid: opemail, name: opname,
    mobile: opmobile , password: oppass, cloud_enabled: cloud_analyser_enabled, ecg_type: changed_operator_type,
    is_admin: changed_is_admin, sample_freequency: changed_sampFreq, loglevel: changed_logLevel, sync: changed_sync,
    session_time:  changed_sesTimeout, thermal_printer: changed_tPrinter, user_usage_type: changed_userType,
    spot_moniter: changed_spotMonitor,
    name_enabled: changed_patientName,  age_enabled: changed_patientAge, mobile_enabled: changed_patientPhone,
    QR_code_enabled: changed_qrCode, mode: changed_mode, is_pdf_sync: changed_pdfDenoiseSync,
    country_code: countrycode, qualification: changed_qualification, cityname: city_name,
    speciality: speciality, city_classification: city_classification,
    role: changed_role, region: changed_region};

  //  const body = new FormData();
  //  body.append('operator_id', operator_id);
  //  body.append('operator_name', opname);
  //  body.append('operator_email', opemail);
  //  body.append('operator_phone', opmobile);
  //  body.append('leads', operator_type);
  //  body.append('disable', markas_delete);
  //  body.append('cloud', cloud_analyser_enabled);
  //  body.append('type', userType);

  console.log(resetPasswordPayload);
   return this.http.post(
     this.API_URL + 'add_app_users.php',
     resetPasswordPayload
     )
     .pipe(
       map(
         response => response
       )
     );
  // return ;
   }


  saveFeedback(payload): Observable<any> {
    return this.http.post(
      // this.API_URL + 'savefeedback', payload
      this.API_URL_COMMENT + 'log_mantis_i3.php', payload
    )
      .pipe(
        map(
          response => response
        )
      );
  }
}

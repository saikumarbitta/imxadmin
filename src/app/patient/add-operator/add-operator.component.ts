import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AddOperatorService } from './add-operator.service';
import { MatSnackBar, MatBottomSheetRef, MAT_DIALOG_DATA, MatDialogRef, MatBottomSheet } from '@angular/material';
import { SharedService } from '../../shared/services/shared.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-add-operator',
  templateUrl: './add-operator.component.html',
  styleUrls: ['./add-operator.component.scss']
})
export class AddOperatorComponent implements OnInit {


  operator_id: any;
  name: any;
  emailid: any;
  pass: any;
  mobile: any;
  operator_type: any;
  cloud_analyser_enabled: any;
  markas_delete: any;
  mode: any;
  pdfDenoiseSync: any;
  sync: any;
  logLevel: any;
  sesTimeout = '250';
  country_code = '0';
  sampFreq: any;
  tPrinter: any;
  userType: any;
  spotMonitor: any;
  patientName: any;
  patientAge: any;
  patientPhone: any;
  ecgRep: any;
  qrCode: any;
  city_name = '';

  changed_mobile: any;
  // changed_operator_type: any;
  changed_markas_delete: any;
  // changed_cloud_analyser_enabled: any;

  // changed_mode: any;
  // changed_pdfDenoiseSync: any;
  // changed_sync: any;
  // changed_logLevel: any;
  // changed_sesTimeout: any;
  // changed_sampFreq: any;
  // changed_tPrinter: any;
  // changed_userType: any;
  // changed_spotMonitor: any;
  // changed_patientName: any;
  // changed_patientAge: any;
  // changed_patientPhone: any;
  // changed_ecgRep: any;
  // changed_qrCode: any;

  title = '';
  usertype = '';




  feedbackform: FormGroup;
  routerSubscription: Subscription;
  isFeedbackLoaded = false;
  selectedFile: File;

  imageError = '';
  fileName = '';
    isImageSaved: boolean;
    cardImageBase64: string;
  base64textString: string;

  hospCode: any;
  hospName: any;

  defaultTYPE = '6';
  defaultZERO = '0';
  defaultONE = '1';
  defaultFreq = '122';

  defaultMode = '1';
  defaultpdfsync = '0';
  defaultsync = '0';
  defaultlog = '0';
  defaulttprint = '0';
  defaulttype = '1';
  defaultspotcheck = '0';
  defaultpatname = '0';
  defaultpatage = '0';
  defaultpatphone = '0';
  defaultecgrep = '0';
  defaultqr = '0';
  defaultadm = '0';
  defaulclass = '';
  defaultrole = '';
  defaultreg = '';

  changed_cloud_analyser_enabled = this.defaultZERO;
  changed_operator_type = this.defaultTYPE;
  changed_is_admin = this.defaultZERO;
  changed_mode = this.defaultZERO;
  changed_pdfDenoiseSync = this.defaultZERO;
  changed_sync = this.defaultZERO;
  changed_logLevel = this.defaultZERO;
  // changed_sesTimeout = this.defaultZERO;
  changed_sampFreq = this.defaultFreq;
  changed_tPrinter = this.defaultZERO;
  changed_userType = this.defaultONE;
  changed_spotMonitor = this.defaultZERO;
  changed_patientName = this.defaultZERO;
  changed_patientAge = this.defaultZERO;
  changed_patientPhone = this.defaultZERO;
  changed_ecgRep = this.defaultZERO;
  changed_qrCode = this.defaultZERO;
  changed_qualification = '';
  changed_speciality = '';
  changed_city_classification = '';
  changed_role = '';
  changed_region = '';

  qualificationList: string[] = [
    'MBBS',
    'MD',
    'MBBS, MD',
    'BAMS',
    'DM',
    'BHMS',
    'BUMS',
    'MD, MRCP',
    'MBBS DGO',
    'MBA',
    'MBBS, DNB',
    'RMP',
    'MD DNB',
    'MBBS, MS',
    'MS',
    'DMLT',
    'MBBS,DM',
    'MD, IDCC',
    'ICU Technician',
    'MD DM',
    'MBBS ,DNB',
    'MBBS, DIP CARD',
    'DNB,CTVS',
    'DHMS'
  ];

  specialityList: string[] = [
    'Cardiologist',
    'Consultant Physician',
    'Paediatrics',
    'General Practitioner-Ayurveda',
    'General Practitioner-Homeopathic',
    'Marketing',
    'General Practitioner',
    'Unani Medicine and Surgery',
    'Hospital Super',
    'Medical Officer',
    'General Physician',
    'Rural medical practitioner',
    'General Physician & Surgery',
    'General Physician & Master of Surgery',
    'Master of Surgery',
    'ICU Technician',
    'Diabetology',
    'General Practitioner-Unani',
    'Pulmonologist ',
    'Diploma in medical Laboratory',
    'Cardiologist & Cardio thoracic &Vascular Surg'
  ];

  constructor(
    private fb: FormBuilder,
     private fbservice: AddOperatorService,
      private snackbar: MatSnackBar,
      private router: Router,
      private shared: SharedService,
      private activatedRoute: ActivatedRoute,
      private _bottomSheet: MatBottomSheet) {

        if (router.url.indexOf('page') === -1) {
          this.routerSubscription = router.events.pipe(
            filter(event => event instanceof NavigationEnd)
          ).subscribe((event: NavigationEnd) => {
            this.shared.titleSource.next('Feedback');
          });
        }

       }

  ngOnInit() {
    const selectedOperator =  this.shared.getSelectedOperator();
    this.hospCode = this.activatedRoute.snapshot.params.code;
    this.hospName = this.activatedRoute.snapshot.params.hospital;

    // if (this.shared.getSelectedTab() === 0) {
      this.title = 'Add Operator';

      // this.setOperator(selectedOperator.operator_id , selectedOperator.name, selectedOperator.emailid,
      //   selectedOperator.mobile, selectedOperator.operator_type, selectedOperator.cloud_analyser_enabled,
      //   selectedOperator.markas_delete, selectedOperator.mode, selectedOperator.pdfDenoiseSync, selectedOperator.sync,
      //   selectedOperator.logLevel, selectedOperator.sesTimeout,  selectedOperator.sampFreq, selectedOperator.tPrinter,
      //   selectedOperator.userType, selectedOperator.spotMonitor, selectedOperator.patientName, selectedOperator.patientAge,
      //   selectedOperator.patientPhone, selectedOperator.ecgRep, selectedOperator.qrCode
      // );
    // }

    this.feedbackform = this.fb.group({
      opname: ['', Validators.required],
      opemail: ['', Validators.required],
      oppass: ['', Validators.required],
      opmobile: ['', Validators.required],
      leads: this.operator_type,
      clouds: [''],
      opdisabled: [''],
      opsesTimeout: [''],
      opcountry_code: [''],
      ophospital_code: [''],
      ophospital_name: [''],
      opterms_condition: [''],
      oplast_reset_password_time: [''],
      opcity_name: ['']
    });
  }

//   setOperator(operator_id , name, emailid,
//     mobile, operator_type, cloud_analyser_enabled,
//     markas_delete,
//     mode, pdfDenoiseSync, sync,
//     logLevel, sesTimeout, sampFreq,
//     tPrinter, userType, spotMonitor,
//     patientName, patientAge, patientPhone,
//     ecgRep, qrCode) {
//       this.operator_id = operator_id;
//       this.name = name;
//       this.emailid = emailid;
//       this.mobile = mobile;
//       this.operator_type = new FormControl(operator_type);
//       this.cloud_analyser_enabled = new FormControl(cloud_analyser_enabled);
//       this.markas_delete = new FormControl(markas_delete);

//       this.mode = new FormControl(mode);
//     this.pdfDenoiseSync = new FormControl(pdfDenoiseSync);
//     this.sync = new FormControl(sync);
//     this.logLevel = new FormControl(logLevel);
//     this.sesTimeout = sesTimeout;

//     this.sampFreq = new FormControl(sampFreq);
//     this.tPrinter = new FormControl(tPrinter);
//     this.userType = new FormControl(userType);
//     this.spotMonitor = new FormControl(spotMonitor);
//     this.patientName = new FormControl(patientName);

//     this.patientAge = new FormControl(patientAge);
//     this.patientPhone = new FormControl(patientPhone);
//     this.ecgRep = new FormControl(ecgRep);
//     this.qrCode = new FormControl(qrCode);

//       this.changed_mobile = mobile;
//       this.changed_operator_type = operator_type ;
//       this.changed_markas_delete = markas_delete ;
//       this.changed_cloud_analyser_enabled = cloud_analyser_enabled ;

//       this.changed_mode = mode ;
//       this.changed_pdfDenoiseSync = pdfDenoiseSync ;
//       this.changed_sync = sync ;
//       this.changed_logLevel = logLevel ;
//       this.changed_sesTimeout = sesTimeout ;

//       this.changed_sampFreq = sampFreq ;
//       this.changed_tPrinter = tPrinter ;
//       this.changed_userType = userType ;
//       this.changed_spotMonitor = spotMonitor ;
//       this.changed_patientName = patientName ;

//       this.changed_patientAge = patientAge ;
//       this.changed_patientPhone = patientPhone ;
//       this.changed_ecgRep = ecgRep ;
//       this.changed_qrCode = qrCode  ;


// }
change_is_admin (value) {
  this.changed_is_admin = value;
}
change_qrCode (value) {
  this.changed_qrCode = value;
}

change_ecgRep (value) {
  this.changed_ecgRep = value;
}

change_patientAge  (value) {
  this.changed_patientAge = value;
}

change_patientName (value) {
  this.changed_patientName = value;
}

change_spotMonitor (value) {
  this.changed_spotMonitor = value;
}
change_userType (value) {
  this.changed_userType = value;
}

change_tPrinter (value) {
  this.changed_tPrinter = value;
}

change_sampFreq (value) {
  this.changed_sampFreq = value;
}
change_sesTimeout (value) {
  // this.changed_sesTimeout = value;
}
change_logLevel(value) {
  this.changed_logLevel = value;
}

change_sync (value) {
  this.changed_sync = value;
}

change_pdf (value) {
  this.changed_pdfDenoiseSync = value;
}

change_patientPhone (value) {
  this.changed_patientPhone = value;
}

change_mode (value) {
  this.changed_mode  = value;
}

change_mob (value) {
  this.changed_mobile  = value;
}

change_op_type (value) {
  // console.log(value);
  this.changed_operator_type = value;
}

change_op_markas_delete (value) {
  // console.log(value);
  this.changed_markas_delete = value;
}

change_op_cloud_analyser_enabled (value) {
  // console.log(value);
  this.changed_cloud_analyser_enabled = value;
}

change_qualification (value) {
  this.changed_qualification = value;
}

change_speciality (value) {
  this.changed_speciality = value;
}

change_city_classification (value) {
  this.changed_city_classification = value;
}

change_role (value) {
  this.changed_role = value;
}

change_region (value) {
  this.changed_region = value;
}

OnInput(event: any) {
  // this.changedopmobile = event.target.value;
  }

  sendFeedBack(form: FormGroup) {

      this.fbservice.addOperator('operator', this.hospCode, form.value.opemail, form.value.opname,
        form.value.opmobile ,  form.value.oppass, this.changed_cloud_analyser_enabled, this.changed_operator_type,
        this.changed_is_admin, this.changed_sampFreq, this.changed_logLevel, this.changed_sync,
        form.value.opsesTimeout === '' || undefined ? this.sesTimeout : form.value.opsesTimeout,
        this.changed_tPrinter, this.changed_userType, this.changed_spotMonitor,
        this.changed_patientName, this.changed_patientAge, this.changed_patientPhone, this.changed_qrCode,
        this.changed_mode, this.changed_pdfDenoiseSync,
        form.value.opcountry_code === '' || undefined ? this.country_code : form.value.opcountry_code,
        this.changed_qualification,
        form.value.opcity_name === '' || undefined ? this.city_name : form.value.opcity_name,
        this.changed_speciality, this.changed_city_classification,
        this.changed_role, this.changed_region)
        .subscribe( response => {
          console.log(response);
          if (response['Result'] === 'Success') {
            this.snackbar.open(
              this.name + ' updated successfully!',
              '',
              {
                duration: 3000
              }
            );
            // this.dialogref.close();
            this.router.navigate(['hospital']);
          } else {
            this.snackbar.open(
              'Error' + response.message,
              '',
              {
                duration: 3000
              }
            );
            // this.dialogref.close();
          }
        });

  }

close() {
  this.router.navigate(['hospital']);
}

}




import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutComponent } from '../shared/components/layout/layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from '../auth/auth-guard.service';
import { FeedbackComponent } from './feedback/feedback.component';


const routes: Routes = [
  {
    path: 'dashboard', component:  LayoutComponent, canActivateChild: [AuthGuard], children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: DashboardComponent},
      { path: 'reset-password', component: FeedbackComponent},
      { path: '**', redirectTo: 'home', pathMatch: 'full' }
    ]
  },
  // { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }

import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FeedbackService } from './feedback.service';
import { MatSnackBar, MatBottomSheetRef, MAT_DIALOG_DATA, MatDialogRef, MatBottomSheet } from '@angular/material';
import { SharedService } from 'src/app/shared/services/shared.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {


  feedbackform: FormGroup;
  routerSubscription: Subscription;
  isFeedbackLoaded = false;
  selectedFile: File;

  imageError = '';
  fileName = '';
    isImageSaved: boolean;
    cardImageBase64: string;
  base64textString: string;
  usertype = 'operator';
  constructor(
    private fb: FormBuilder,
     private fbservice: FeedbackService,
      private snackbar: MatSnackBar,
      private router: Router,
      private shared: SharedService,
      private activatedRoute: ActivatedRoute,
      private _bottomSheet: MatBottomSheet) {

        if (router.url.indexOf('page') === -1) {
          this.routerSubscription = router.events.pipe(
            filter(event => event instanceof NavigationEnd)
          ).subscribe((event: NavigationEnd) => {
            this.shared.titleSource.next('Feedback');
          });
        }

       }

  ngOnInit() {
    this.feedbackform = this.fb.group({
      mailid: ['', Validators.required],
      // oldPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      confirmPassword: ['', Validators.required],
    });
  }

  // cancel() {
  //   this.router.navigate(['feedback']);
  // }

  change_user_type (value) {
    console.log(value);
    this.usertype = value;
  }

  sendFeedBack (form: FormGroup) {
    const email = form.value.mailid;
    // const oldpwd = form.value.oldPassword;
    const newpwd = form.value.newPassword;
    const confirmpwd = form.value.confirmPassword;

    // if (oldpwd !== newpwd) {
      if (newpwd === confirmpwd) {
        this.fbservice.changePassword(this.usertype, email, newpwd)
          .subscribe(
            response => {
              if (response.response === 'success') {
                this.snackbar.open(
                  response.message ,
                  '',
                  {
                    duration: 3000
                  }
                );
                // this.router.navigate(['/auth/sigin']);
              } else {
                this.snackbar.open(
                  '',
                  'Invalid password. Please check and try again!!!',
                  {
                    duration: 3000
                  }
                );
               }
            }
          );
      } else {
        this.snackbar.open(
          'Password and Confirm password does not match, please try again!!!',
          '',
          {
            duration: 3000
          }
        );
      }
    // } else {
    //   this.snackbar.open(
    //     'New password and Old password are same, please try with different password!!!',
    //     '',
    //     {
    //       duration: 3000
    //     }
    //   );
    // }
  }

  onClickContact() {
    // this._bottomSheet.open(BottomSheetContactComponent, {
    //   panelClass: 'custom-width',
    //  // data: { dataVtrMap: true }
    // });
  }

}



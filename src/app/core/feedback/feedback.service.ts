import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  private API_URL = environment.API_URL;
  private API_URL_COMMENT = environment.API_URL_COMMENT;

  constructor(private http: HttpClient) { }


  changePassword(usertype, email, newpwd): Observable<any> {
    const resetPasswordPayload = {type: usertype,
                                  username: email,
                                  newpassword: newpwd };
    console.log(resetPasswordPayload);
    return this.http.post(
      this.API_URL + 'reset_password.php',
      resetPasswordPayload
      )
      .pipe(
        map(
          response => response
        )
      );


  }
}

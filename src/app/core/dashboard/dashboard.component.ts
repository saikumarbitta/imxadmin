import { Component, OnInit, OnDestroy, OnChanges } from '@angular/core';
import { DashboardService } from './dashboard.service';
import { IDashboard } from './dashboard.model';
import { Subscription } from 'rxjs';
import { SharedService } from '../../shared/services/shared.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ScreenSizeService } from '../../shared/services/screen-size.service';
import {  Color } from 'ng2-charts';
import { ChartType , Label , MultiDataSet} from 'chart.js';
import { NONE_TYPE } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  public options: any = {
    cutoutPercentage: 65,
    legend:
    {
    }
  };

    public doughnutChartLabels: Label[] = [];
    public doughnutChartData: MultiDataSet = [1] ;
    public doughnutChartType: ChartType = 'doughnut';
    doughbackgroundColor: Color[] = [{backgroundColor: ['#F3F5F8']}];


  public barChartLabels: string[] = [];
  public barChartType = 'bar';
  public barChartLegend = true;

  is_thirdparty_doctor = localStorage.getItem('is_thirdparty_doctor');

  public barChartData: any[] = [
    { data: [], label: 'TOTAL RECORDS' },
    { data: [], label: 'SEVERE RECORD' }
  ];

  public barChartOptions: any = {
    responsive: true,
    /* legend: {
      labels: {
        fontColor: '#222',
        fontSize: 10
      }
    }, */
    animation: {
      onComplete: function () {
        const chartInstance = this.chart,
          ctx = chartInstance.ctx;
        ctx.textAlign = 'center';
        ctx.fillStyle = 'rgb(0, 0, 0)',
          this.data.datasets.forEach(function (dataset, i) {
            const meta = chartInstance.controller.getDatasetMeta(i);
            meta.data.forEach(function (bar, index) {
              const data = dataset.data[index];
              ctx.fillText(data, bar._model.x, bar._model.y - 3);
            });
          });
      }
    }
  };

  public barChartLabels2: string[] = ['Severe', 'Moderate', 'Low'];
  public barChartType2 = 'bar';
  public barChartLegend2 = true;

  public barChartData2: any[] = [
    {
      label: 'Reviewed',
      data: [40, 47, 44]
    },
    {
      label: 'Not Reviewed',
      data: [10, 27, 34]
    }
  ];

  public barChartOptions2: any = {
    animation: {
      duration: 10,
      onComplete: function () {
        const chartInstance = this.chart,
          ctx = chartInstance.ctx;
        ctx.textAlign = 'center';
        ctx.fillStyle = 'rgb(0, 0, 0)',
          this.data.datasets.forEach(function (dataset, i) {
            const meta = chartInstance.controller.getDatasetMeta(i);
            meta.data.forEach(function (bar, index) {
              const data = dataset.data[index];
              ctx.fillText(data, bar._model.x, bar._model.y - 3);
            });
          });
      }
    },
    tooltips: {
      mode: 'label',
      callbacks: {
        label: function (tooltipItem, data) {
          return data.datasets[tooltipItem.datasetIndex].label + ': ' + numberWithCommas(tooltipItem.yLabel);
        }
      }
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: { display: false },
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          callback: function (value) { return numberWithCommas(value); },
        },
      }],
    }, // scales
    legend: { display: false }
  };

  public dashboardData: IDashboard;

  // Dashboard Subscription
  private dashboardSubscription: Subscription;

  constructor(
    public dashboardService: DashboardService,
    public shared: SharedService,
    private router: Router,
    private route: ActivatedRoute,
    public screen: ScreenSizeService
  ) { }


  chartIndex = 0;

  // Total Patient Chart
  public totalPatientChartData: any[] = [];
  public totalPatientChartLabels: string[] = ['FEMALE', 'MALE', 'OTHERS'];

  // Comments Chart
  public commentsChartLabels: string[] = ['REVIEWED', 'PENDING'];
  public commentsChartData: any[] = [];


  // Risk Chart
  public docClassificationChart: any[] = [];
  public aiClassificationChart: any[] = [];

  public riskChartLabelsDC: string[] = ['ACUTE', 'INVESTIGATION', 'MEDICATION', 'NORMAL', 'OTHERS'];
  public riskChartLabelsAI: string[] = ['HIGH', 'MODERATE', 'LOW', 'NORMAL', 'UNCLASSIFIED'];
  public riskChartData: any[] = [];



  // public docClassificationChartPastWeekOuter: any[] = [];
  // public docClassificationChartPastWeekInner: any[] = [11, 19, 5, 15, 7, 33, 20, 30];
  public docClassificationChartPastWeekDataset: any[] = [];



  /* public aiClassificationChartPastWeekOuter: any[] = [30, 20, 40, 50];
  public aiClassificationChartPastWeekInner: any[] = [11, 19, 5, 15, 7, 33, 20, 30]; */
  public aiClassificationChartPastWeekDataset: any[] = [
  ];

  public aiClassificationChartPastWeek: any[] = [];




  public chartType = 'doughnut';

  public chartOptionsOverall1: any = {
    responsive: true,
    cutoutPercentage: 68,
    animation: {
      onComplete: function () {
        const chartInstance = this.chart,
          ctx = chartInstance.ctx;
        ctx.textAlign = 'center';
        ctx.fillStyle = 'rgb(0, 0, 0)',
        ctx.fillText('OVERALL', 160, 90);
      }
    },
    maintainAspectRatio: false,
    legend: { display: false },

    pieceLabel: {
      render: function (args) {
        const label = args.label,
          value = args.value;
        return value;
      },
      fontColor: '#fff',
      fontSize: 14
    }
  };

  public chartOptionsOverall2: any = {
    responsive: true,
    tooltips: false,
    cutoutPercentage: 68,
    animation: {
      onComplete: function () {
        const chartInstance = this.chart,
          ctx = chartInstance.ctx;
        ctx.textAlign = 'center';
        // ctx.font('10px Calibri (Body)');
//         ctx.font = '20px Georgia';
// ctx.fillText('Hello World!', 10, 50);
        ctx.font = '10px Calibri (Body)';
        ctx.fillStyle = 'rgb(10, 10, 10)',
        ctx.fillText('OVERALL', 160, 90);
        ctx.font = '10px Calibri';
        // var gradient = ctx.createLinearGradient(0, 0, 12, 0);
        // gradient.addColorStop('0',' magenta');
        // gradient.addColorStop('0.5', 'blue');
        // gradient.addColorStop('1.0', 'red');
        ctx.fillStyle = '#D3D3D3';
        ctx.fillText('NO RECORD FOUND', 160, 105);
      }
    },
    maintainAspectRatio: false,
    legend: { display: false },

    pieceLabel: {

      render: function (args) {
        const label = args.label,
          value = args.value;
        return value;
      },
      fontColor: '#fff',
      fontSize: 14
    }
  };

  public chartOptionsHr1: any = {
    responsive: true,
    cutoutPercentage: 68,
    animation: {
      onComplete: function () {
        const chartInstance = this.chart,
          ctx = chartInstance.ctx;
        ctx.textAlign = 'center';
        ctx.fillStyle = 'rgb(0, 0, 0)',
        ctx.fillText('PAST 24 HOURS', 160, 90);
      }
    },
    maintainAspectRatio: false,
    legend: { display: false },

    pieceLabel: {

      render: function (args) {
        const label = args.label,
          value = args.value;
        return value;
      },
      fontColor: '#fff',
      fontSize: 14
    }
  };

  public chartOptionsHr2: any = {
    responsive: true,
    tooltips: false,
    cutoutPercentage: 68,
    animation: {
      onComplete: function () {
        const chartInstance = this.chart,
          ctx = chartInstance.ctx;
        ctx.textAlign = 'center';
        // ctx.font('10px Calibri (Body)');
//         ctx.font = '20px Georgia';
// ctx.fillText('Hello World!', 10, 50);
        ctx.font = '10px Calibri (Body)';
        ctx.fillStyle = 'rgb(10, 10, 10)',
        ctx.fillText('PAST 24 HOURS', 160, 90);
        ctx.font = '10px Calibri';
        // var gradient = ctx.createLinearGradient(0, 0, 12, 0);
        // gradient.addColorStop('0',' magenta');
        // gradient.addColorStop('0.5', 'blue');
        // gradient.addColorStop('1.0', 'red');
        ctx.fillStyle = '#D3D3D3';
        ctx.fillText('NO RECORD FOUND', 160, 105);
      }
    },
    maintainAspectRatio: false,
    legend: { display: false },

    pieceLabel: {

      render: function (args) {
        const label = args.label,
          value = args.value;
        return value;
      },
      fontColor: '#fff',
      fontSize: 14
    }
  };

  public chartOptions1Notused: any = {
    responsive: true,
    cutoutPercentage: 70,
    maintainAspectRatio: false,
    /* legend: {
      labels: {
        fontColor: '#222',
        fontSize: 10
      }
    }, */
    legend: { display: false },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          const dataset = data.datasets[tooltipItem.datasetIndex];
          const index = tooltipItem.index;
          return dataset.labels[index] + ': ' + dataset.data[index];
        }
      }
    },
    pieceLabel: {
      render: function (args) {
        const label = args.label,
          value = args.value;
        return value;
      },
      fontColor: '#fff',
      fontSize: 14
    }
  };

  isDashboardLoadedNotLoaded = true;


  // events
  public chartClicked(e: any, type, hospital): void {
    let chartType: string;
    if (e.active.length > 0) {
      switch (true) {
        case (e.active[0]._datasetIndex === 0 && e.active[0]._index === 0 && type === 'AI'): {
          chartType = 'AI_High';
          break;
        }
        case (e.active[0]._datasetIndex === 0 && e.active[0]._index === 1 && type === 'AI'): {
          chartType = 'AI_Moderate';
          break;
        }
        case (e.active[0]._datasetIndex === 0 && e.active[0]._index === 2 && type === 'AI'): {
          chartType = 'AI_Low';
          break;
        }
        case (e.active[0]._datasetIndex === 0 && e.active[0]._index === 3 && type === 'AI'): {
          chartType = 'AI_Normal';
          break;
        }
        case (e.active[0]._datasetIndex === 0 && e.active[0]._index === 4 && type === 'AI'): {
          chartType = 'AI_Unclassified';
          break;
        }
        case (e.active[0]._datasetIndex === 0 && e.active[0]._index === 0 && type === 'AIP'): {
          chartType = 'AI_High_Past_24_Hours';
          break;
        }
        case (e.active[0]._datasetIndex === 0 && e.active[0]._index === 1 && type === 'AIP'): {
          chartType = 'AI_Moderate_Past_24_Hours';
          break;
        }
        case (e.active[0]._datasetIndex === 0 && e.active[0]._index === 2 && type === 'AIP'): {
          chartType = 'AI_Low_Past_24_Hours';
          break;
        }
        case (e.active[0]._datasetIndex === 0 && e.active[0]._index === 3 && type === 'AIP'): {
          chartType = 'AI_Normal_Past_24_Hours';
          break;
        }
        case (e.active[0]._datasetIndex === 0 && e.active[0]._index === 4 && type === 'AIP'): {
          chartType = 'AI_Unclassified_Past_24_Hours';
          break;
        }
        case (e.active[0]._datasetIndex === 1 && e.active[0]._index === 0 && type === 'AIP'): {
          chartType = 'Not_Reviewed_Severe';
          break;
        } case (e.active[0]._datasetIndex === 1 && e.active[0]._index === 2 && type === 'AIP'): {
          chartType = 'Not_Reviewed_Moderate';
          break;
        }
        case (e.active[0]._datasetIndex === 1 && e.active[0]._index === 4 && type === 'AIP'): {
          chartType = 'Not_Reviewed_Low';
          break;
        } case (e.active[0]._datasetIndex === 1 && e.active[0]._index === 6 && type === 'AIP'): {
          chartType = 'Not_Reviewed_Normal';
          break;
        }
        case (e.active[0]._datasetIndex === 0 && e.active[0]._index === 0 && type === 'DC'): {
          chartType = 'DC_Acute';
          break;
        }
        case (e.active[0]._datasetIndex === 0 && e.active[0]._index === 1 && type === 'DC'): {
          chartType = 'DC_Investigation';
          break;
        }
        case (e.active[0]._datasetIndex === 0 && e.active[0]._index === 2 && type === 'DC'): {
          chartType = 'DC_Medication';
          break;
        } case (e.active[0]._datasetIndex === 0 && e.active[0]._index === 3 && type === 'DC'): {
          chartType = 'DC_Normal';
          break;
        } case (e.active[0]._datasetIndex === 0 && e.active[0]._index === 4 && type === 'DC'): {
          chartType = 'DC_Others';
          break;
        }
        case (e.active[0]._datasetIndex === 0 && e.active[0]._index === 0 && type === 'DCP'): {
          chartType = 'DC_Acute_Past_24_Hours';
          break;
        }
        case (e.active[0]._datasetIndex === 0 && e.active[0]._index === 1 && type === 'DCP'): {
          chartType = 'DC_Investigation_Past_24_Hours';
          break;
        }
        case (e.active[0]._datasetIndex === 0 && e.active[0]._index === 2 && type === 'DCP'): {
          chartType = 'DC_Medication_Past_24_Hours';
          break;
        }
        case (e.active[0]._datasetIndex === 0 && e.active[0]._index === 3 && type === 'DCP'): {
          chartType = 'DC_Normal_Past_24_Hours';
          break;
        }
        case (e.active[0]._datasetIndex === 0 && e.active[0]._index === 4 && type === 'DCP'): {
          chartType = 'DC_Others_Past_24_Hours';
          break;
        }
        case (e.active[0]._datasetIndex === 1 && e.active[0]._index === 0 && type === 'DCP'): {
          chartType = 'Not_Referred_Severe';
          break;
        }
        case (e.active[0]._datasetIndex === 1 && e.active[0]._index === 2 && type === 'DCP'): {
          chartType = 'Not_Referred_Moderate';
          break;
        } case (e.active[0]._datasetIndex === 1 && e.active[0]._index === 4 && type === 'DCP'): {
          chartType = 'Not_Referred_Low';
          break;
        }
        case (e.active[0]._datasetIndex === 1 && e.active[0]._index === 6 && type === 'DCP'): {
          chartType = 'Not_Referred_Normal';
          break;
        }
        default: {
          console.log('Do Nothing...');
          break;
        }
      }

      if (chartType) {
        this.router.navigate(
          ['patient/patient-list/', chartType, type, this.is_thirdparty_doctor === '1' ? '' : hospital ? hospital : '']
        );
      }
    }


  }

  public chartHovered(e: any): void {
    console.log(e);
  }


  ngOnInit(): void {

    this.storeFcmDetails () ;
    // setting up page title
    this.shared.titleSource.next('Dashboard');
    this.getDashboardData();
  }


  storeFcmDetails () {
    // console.log('ID->' + this.shared.fcmId);
    if (this.shared.fcmId) {
      // this.msgService.updateToken(this.shared.fcmId, this.shared.doctorId);
    }
  }

  getDashboardData() {
    this.dashboardSubscription = this.dashboardService.getDashboardData(this.shared.doctorId).subscribe(
      dbdata => {

        this.dashboardData = dbdata;

        if (this.dashboardData) {
          this.isDashboardLoadedNotLoaded = false;
        }

        this.dashboardData.hospitalData.forEach((element, i) => {

          const commentsCount = [];
          // fill gender chart
          this.totalPatientChartData[i] = [].concat(...element.gender);


          // fill comments chart
          commentsCount.push(element.commented ? element.commented : 0);
          commentsCount.push(element.uncommented ? element.uncommented : 0);
          this.commentsChartData.push(commentsCount);

          // fill risk chart
          this.riskChartData[i] = [].concat(...element.risk);

          // fill doc_overall_count chart
          this.docClassificationChart[i] = [].concat(...element['doc_overall_count']);
          this.aiClassificationChart[i] = [].concat(...element['AI_risk']);

          // outer chart doc
          this.docClassificationChartPastWeekDataset[i] = [{
            data: element['doc_week_count']['doc_count_week'],
            label: 'Doctor Overall Classification',
            labels: ['ACUTE', 'INVESTIGATION', 'MEDICATION', 'NORMAL', 'OTHERS']
          },
          /* {
            data: element['doc_week_count']['doc_refer_week'],
            label: 'Doctor Past Week Classification',
            labels: ['NOT REFERRED', 'REFERRED', 'NOT REFERRED', 'REFERRED', 'NOT REFERRED', 'REFERRED', 'NOT REFERRED', 'REFERRED']
          } */];

          this.aiClassificationChartPastWeekDataset[i] = [
            /*{
            data: element['AI_week_risk']['AI_count_week'],
            label: 'AI Overall Classification',
            labels: ['SEVERE', 'MODERATE', 'LOW', 'NORMAL']
          },*/
          {
            data: element['AI_week_risk']['AI_review_week'],
            label: 'AI Past Week Classification',
            labels: ['NOT REVIEWED', 'REVIEWED', 'NOT REVIEWED', 'REVIEWED', 'NOT REVIEWED',
                    'REVIEWED', 'NOT REVIEWED', 'REVIEWED', 'UNCLASSIFIED']
          }];
          // this.docClassificationChartPastWeekDataset[1] = ;

          // bar graph
          this.barChartLabels = element['month_count']['month_name'];
          this.barChartData[0].data = this.minusOne(element['month_count']['months_value']);
          this.barChartData[1].data = this.minusOne(element['month_count']['risk_value']);

        });


        this.docClassificationChartPastWeekDataset = this.docClassificationChartPastWeekDataset.slice();
        this.aiClassificationChartPastWeekDataset = this.aiClassificationChartPastWeekDataset.slice();
        this.docClassificationChart  = this.docClassificationChart.slice();
        this.aiClassificationChart = this.aiClassificationChart.slice();

        this.totalPatientChartData = this.totalPatientChartData.slice();
        this.totalPatientChartLabels = this.totalPatientChartLabels.slice();

        this.barChartLabels = this.barChartLabels.slice();
        this.barChartData = this.barChartData.slice();
      }
    );
  }

  minusOne(arrayValues) {
    const returnArray = [];
    arrayValues.forEach(element => {
      element = element - 1;
      returnArray.push(element);
    });
    return returnArray;
  }

  nextChart(chart) {
    const nextChartValue = chart + 1;
    if (this.dashboardData.hospitalData.length > nextChartValue) {
      this.chartIndex = nextChartValue;
    }
  }

  previousChart(chart) {
    const previousChartValue = chart - 1;
    if (previousChartValue >= 0 ) {
      this.chartIndex = previousChartValue;
    }
  }

  ngOnDestroy(): void {
    if (this.dashboardSubscription) {
      this.dashboardSubscription.unsubscribe();
    }
    localStorage.removeItem('pageTitle');
  }
}



function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

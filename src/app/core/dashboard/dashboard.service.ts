import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IDashboard } from './dashboard.model';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { NGXLogger } from 'ngx-logger';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  private API_URL = environment.API_URL;

  // colors for chart
  public totalPatientChartColor: Array<any> = [{ backgroundColor: ['#FF5AA1', '#0090DC', '#9F7BCD', '#424949'] }];
  public commentsChartColor: Array<any> = [{ backgroundColor: ['#32CD32', '#F98700'] }];
  public barChartDataColor: any[] = [
    {
      backgroundColor: ['#035D9B', '#035D9B', '#035D9B', '#035D9B', '#035D9B']
    },
    {
      backgroundColor: ['#dd4b39', '#dd4b39', '#dd4b39', '#dd4b39', '#dd4b39']
    }
  ];

  public barChartDataColorRight: any[] = [
    {
      backgroundColor: ['#00A17D', '#035D9B', '#035D9B', '#035D9B', '#035D9B']
    },
    {
      backgroundColor: ['#D14E54', '#dd4b39', '#dd4b39', '#dd4b39', '#dd4b39']
    }
  ];



  public riskChartColorLeft: Array<any> = [{ backgroundColor: ['#dd4b39', '#F4874B', '#87ceeb', '#00A85B', '#EC3B83'] }];


  public docClassificationChartColor: Array<any> = [{
    backgroundColor: ['#dd4b39', '#F4874B', '#87ceeb', '#00A85B' , '#EC3B83']
  },
  {
    backgroundColor: [
      '#dd4b39', 'rgba(0, 0, 0, 0)', '#F4874B', 'rgba(0, 0, 0, 0)',
      '#87ceeb', 'rgba(0, 0, 0, 0)', '#00A85B', 'rgba(0, 0, 0, 0)',
      '#EC3B83', 'rgba(0, 0, 0, 0)'
    ]
  }
  ];

  public aiClassificationChartColor: Array<any> = [{
    backgroundColor: ['#dd4b39', '#F4874B', '#87ceeb', '#00A85B', '#EC3B83']
  },
  {
    backgroundColor: [
      '#dd4b39', 'rgba(0, 0, 0, 0)', '#F4874B', 'rgba(0, 0, 0, 0)',
      '#87ceeb', 'rgba(0, 0, 0, 0)', '#00A85B', 'rgba(0, 0, 0, 0)',
      '#EC3B83', 'rgba(0, 0, 0, 0)'
    ]
  }
  ];

  public riskChartColorRight: Array<any> = [{ backgroundColor: ['#D14E54', '#FF8823', '#007680', '#BEC0C3', '#EC3B83'] }];

  constructor(private http: HttpClient,
    private logger: NGXLogger) { }

  getDashboardData(doctor): Observable<IDashboard> {
    return this.http.post<IDashboard>(
      this.API_URL + 'patientfordoctor',
      { 'doctor_id': doctor })
      .pipe(
        map(r => r)
      );
  }

}

export interface IDashboard {
    doctorId: string;
    doctorName: string;
    hospitalCount: number;
    hospitalData: IHopsitalData[];
    totalCommentedPatients: number;
    totalNumberOfPatients: any;
    totalTaggedPatients: number;
    totalUnCommentedPatients: number;
    devices: any;
    emergencypending: any;
    normalpending: any;
    notnormalpending: any;
    totalsevere: any;
    totalmoderate: any;
    totallow: any;
}


export interface IHopsitalData {
    commented: number;
    gender: any[];
    hospital_code: string;
    hospital_name: string;
    risk: any[];
    tagged: number;
    total_patients: number;
    uncommented: number;
}


export interface IFormValues {
    'doctor_id': string;
    'page': number;
    'isSearch': string;
    'searchtxt': string;
    'searchPatientID': string;
    'phone': string;
    'hospital_code': string;
    'start_date': any;
    'end_date': any;
    'oprator_id': string;
    'filter': string;
    'sort': string;
    'export': number;
    'status': string;
}

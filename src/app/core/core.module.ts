import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardComponent } from './dashboard/dashboard.component';
import { MaterialModule } from '../angular-material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../shared/shared.module';
import { CoreRoutingModule } from './core-routing.module';
import { ChartsModule } from 'ng2-charts';
import 'chart.piecelabel.js';
import { FeedbackComponent } from './feedback/feedback.component';




@NgModule({
  declarations: [
    DashboardComponent,
    FeedbackComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SharedModule,
    CoreRoutingModule,
    ChartsModule
  ],
  exports: [],
  entryComponents: [
  ]
})
export class CoreModule { }



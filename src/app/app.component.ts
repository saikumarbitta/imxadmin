import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatBottomSheet } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  message;

  constructor(
    private bottom: MatBottomSheet
    ) {}

  ngOnInit() {

  }
}

import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate, CanActivateChild {

  token: string;

  constructor(private router: Router, private cookieService: CookieService ) {}

  canActivate() {
    return this.getToken();
  }

  canActivateChild() {
    return this.getToken();
  }

  getToken() {
    const cookietoken = this.cookieService.get('currentUser');
    this.token = cookietoken;
    if (this.token !== '') {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }

}

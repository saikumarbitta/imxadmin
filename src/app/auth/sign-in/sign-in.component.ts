

import { Component, OnInit, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { ScreenToggle } from '../../shared/services/screen-toggle.service';
import { MatBottomSheet, MatBottomSheetRef, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { environment } from '../../../environments/environment';
import { ScreenSizeService } from '../../shared/services/screen-size.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  active: Boolean;
  model: any = {};
  alertResponse: string;
  uhid: any;
  doctorId: any;
  username = localStorage.getItem('careway-doctor-usrname') === 'null' || localStorage.getItem('careway-doctor-usrname') === ''
    ? '' : localStorage.getItem('careway-doctor-usrname');
  hispassword = localStorage.getItem('careway-doctor-usrpwd') === 'null' || localStorage.getItem('careway-doctor-usrpwd') === ''
    ? '' : localStorage.getItem('careway-doctor-usrpwd');

  isHandset: Observable<boolean> = this.screenSize.isHandset;

  constructor(
    private siginInService: AuthService,
    private screenSize: ScreenSizeService,
    private route: ActivatedRoute,
    private router: Router,
    public screenToggle: ScreenToggle,
    private _bottomSheet: MatBottomSheet
  ) { }

  ngOnInit() {
    localStorage.setItem('iMedrixHomePage', environment.iMedrix_HomePage);
    localStorage.setItem('iMedrixUSAContact', environment.iMedrix_USA_Contact);
    localStorage.setItem('iMedrixIndiaContact', environment.iMedrix_India_Contact);
    localStorage.setItem('iMedrixWhazUpContact', environment.iMedrix_WhazUp_Contact);
    localStorage.setItem('iMedrixEmailContact', environment.iMedrix_Email_Contact);
    this.route.queryParams.subscribe(params => {
      this.uhid = params.uhid;
      this.doctorId = params.doctor;
    });
  }

  focusOutFunction(form: NgForm) {
    if (form.value.signinEmail === '' && form.value.password === '') {
      this.active = false;
    } else {
      this.active = true;
    }
  }

  saveCredentials(event, form: NgForm) {
    const usrname = form.value.signinEmail;
    const password = form.value.password;
    if (event.checked) {
      localStorage.setItem('careway-doctor-usrname', usrname);
      localStorage.setItem('careway-doctor-usrpwd', password);
    } else {
      localStorage.removeItem('careway-doctor-usrname');
      localStorage.removeItem('careway-doctor-usrpwd');
    }
  }

  onSignin(form: NgForm) {
    const email = form.value.signinEmail;
    const password = form.value.password;
    this.siginInService.signinUser(email, password, this.uhid, this.doctorId);
    this.screenToggle.launchIntoFullscreen(document.documentElement);
  }



}


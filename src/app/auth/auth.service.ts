import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { SigninAlertComponent } from '../shared/components/signin-alert/signin-alert.component';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material';
import { environment } from '../../environments/environment';
import { ISignin } from './sign-in/sign-in.model';
import { SharedService } from '../shared/services/shared.service';
import { CookieService } from 'ngx-cookie-service';
import { NGXLogger } from 'ngx-logger';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  alertResponse: string;

  private API_URL = environment.API_URL;

  constructor(
    private router: Router,
    private http: HttpClient,
    public dialog: MatDialog,
    private shared: SharedService,
    private cookieService: CookieService,
    private logger: NGXLogger
  ) { }

  signinUser(email: string, password: string, uhid, doctor) {
    const payload = {
      'emailid': email,
      'password': password
    };

    // this.logger.info(this.loggerService.formateMsg('[Auth Service]' , 38, 'signinUser',
    //                               'Login Initiated for User id : ' + email ));
    return this.http.post<ISignin>(this.API_URL + 'admin_login.php',
      payload).subscribe(
        res => {
    //      console.log(res['User']['username']);
          // console.log(res['response'] === 'success');
          if (res['response'] === 'success') {
              this.router.navigate(['/hospital']);
              this.cookieService.set('currentUser', res['User']['username'], 0.0416667); /*  0.0416667 */

              localStorage.setItem('currentUser', res['User']['username']);
              localStorage.setItem('mobile', res['User']['mobile']);
              localStorage.setItem('emailid', res['User']['emailid']);
      //        console.log( localStorage.getItem('currentUser'));
          } else {
            // this.logger.error(this.loggerService.formateMsg('[Auth Service]' , 38, 'signinUser',
            //                       'Login Failed for User : ' + email ));
            this.dialog.open(SigninAlertComponent, {
              width: '300px'
            });
          }
        }
      );
  }
}

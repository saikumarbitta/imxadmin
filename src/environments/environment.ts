import { NgxLoggerLevel } from 'ngx-logger';

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {

  production: false,
  // API_URL: 'http://192.168.1.154/imedrix_backend/i3Api/doctordashboard/v1/',
  // API_URL_COMMENT: 'http://192.168.1.154/imedrix_backend/i3Api/html/doctordashboard/v1/',
  // RISK_URL: 'https://portal.imedrix.com/risk-analyzer/',
  // API_URL2: 'http://192.168.1.154/imedrix_backend/i3Api/participant/v1/',

  // API_URL: 'https://portal.imedrix.com/i3_IWS/doctordashboard/v1/',
  // API_URL_COMMENT: 'https://portal.imedrix.com/i3_IWS/html/doctordashboard/v1/',
  // RISK_URL: 'https://portal.imedrix.com/risk-analyzer/',
  // API_URL2: 'http://portal.imedrix.com/i3_IWS/participant/v1/',

  // API_URL: 'http://106.51.126.124:82/imedrix_backend/i3Api/doctordashboard/v1/',
  // API_URL_COMMENT: 'http://106.51.126.124:82/imedrix_backend/i3Api/html/doctordashboard/v1/',
  // RISK_URL: 'http://localhost:4210/risk-analyzer/',
  // API_URL2: 'http://106.51.126.124:82/imedrix_backend/i3Api/participant/v1/',

  API_URL: 'http://106.51.126.124:82/imedrix_backend/IWS_AdminPortal/',
  API_URL_COMMENT: 'https://portal.imedrix.com/i3_IWS/html/doctordashboard/v1/',
  RISK_URL: 'https://portal.imedrix.com/risk-analyzer/',
  API_URL2: 'http://portal.imedrix.com/i3_IWS/participant/v1/',


  apiUrl: 'https://portal.imedrix.com/KS_API/V4/sync_i3_log.php', // Replace with local API
  logLevel: NgxLoggerLevel.TRACE, //  warn logs & above will be logged on the browser
  logLevelID: 0,
  serverLogLevel: NgxLoggerLevel.TRACE, // TRACE, DEBUG, INFO, LOG, WARN, ERROR, FATAL and OFF

  versionID: 1.0,
  iMedrix_HomePage: 'http://www.imedrix.com',
  iMedrix_USA_Contact: '1866 463 3749',
  iMedrix_India_Contact: '+91 80 4161 4164',
  iMedrix_WhazUp_Contact: '+91 99 0062 1212',
  iMedrix_Email_Contact: 'support@imedrix.com',

  // mantis
  authToken: 'mwVt_c2LWGCJyvjyCx4R1pjcx_Fr9_Uu',
  projectid: 3,
  projectname: 'i3_1.0',

  firebase: {
    apiKey: 'AIzaSyB5HCm2-V_KEwzfDZUwbHvnIB7KyPobmhY',
    authDomain: 'careway-for-cvd.firebaseapp.com',
    databaseURL: 'https://careway-for-cvd.firebaseio.com',
    projectId: 'careway-for-cvd',
    storageBucket: 'careway-for-cvd.appspot.com',
    messagingSenderId: '1091233228343'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

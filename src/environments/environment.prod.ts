import { NgxLoggerLevel } from 'ngx-logger';

export const environment = {
  production: true,
  // API_URL: 'https://screening.imedrix.com/screeningwebApis/doctordashboard/v1/'
  API_URL: 'https://portal.imedrix.com/IWS_AdminPortal/',
  API_URL_COMMENT: 'https://portal.imedrix.com/i3_IWS/html/doctordashboard/v1/',
  RISK_URL: 'https://portal.imedrix.com/risk-analyzer/',
  API_URL2: 'http://portal.imedrix.com/i3_IWS/participant/v1/',

  apiUrl: 'https://portal.imedrix.com/KS_API/V4/sync_i3_log.php', // Replace with local API
  logLevel: NgxLoggerLevel.OFF, //  warn logs & above will be logged on the browser
  logLevelID: 0,
  serverLogLevel: NgxLoggerLevel.TRACE, // TRACE, DEBUG, INFO, LOG, WARN, ERROR, FATAL and OFF

  versionID: 1.0,
  iMedrix_HomePage: 'http://www.imedrix.com',
  iMedrix_USA_Contact: '1866 463 3749',
  iMedrix_India_Contact: '+91 80 4161 4164',
  iMedrix_WhazUp_Contact: '+91 99 0062 1212',
  iMedrix_Email_Contact: 'support@imedrix.com',

  // mantis
  authToken: 'mwVt_c2LWGCJyvjyCx4R1pjcx_Fr9_Uu',
  projectid: 3,
  projectname: 'i3_1.0',

  firebase: {
    apiKey: 'AIzaSyB5HCm2-V_KEwzfDZUwbHvnIB7KyPobmhY',
    authDomain: 'careway-for-cvd.firebaseapp.com',
    databaseURL: 'https://careway-for-cvd.firebaseio.com',
    projectId: 'careway-for-cvd',
    storageBucket: 'careway-for-cvd.appspot.com',
    messagingSenderId: '1091233228343'
  }
};
